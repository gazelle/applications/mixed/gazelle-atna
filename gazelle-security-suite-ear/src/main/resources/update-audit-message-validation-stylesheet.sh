#!/bin/bash

# Used to update host stylesheet in Audit Message validation logs

QUESTIONNAIRE_DIR='/opt/tls/questionnaire'
XSL_OLD_PATH=$(echo 'https:\/\/gazelle.ihe.net\/gss\/resources\/stylesheet\/auditMessagevalidatorDetailedResult.xsl')
XSL_NEW_PATH=$(echo 'http:\/\/localhost:8080\/gss-dev\/resources\/stylesheet\/auditMessagevalidatorDetailedResult.xsl')

cd $QUESTIONNAIRE_DIR/audit_messages
sudo find -iname *.xml -exec sed -i "s/$XSL_OLD_PATH/$XSL_NEW_PATH/g" {} \;
