INSERT INTO app_configuration (id, value, variable) VALUES (nextval('app_configuration_id_seq'), 'https://gazelle.ihe.net/XSD/IHE/XUA/xmldsig-core-schema.xsd', 'xmldsig_core_schema_location');

-- Update Cipher Suites recording from ORDINAL to STRING --

-- TLS Simulators
-- Create a new column for migrating enum string values
ALTER TABLE tls_simulator_ciphersuites ADD COLUMN new_ciphersuite VARCHAR(128);
-- Migrate ordinal values to String in the new column
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='SSL_RSA_WITH_RC4_128_MD5' WHERE ciphersuite=0;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='SSL_RSA_WITH_RC4_128_SHA' WHERE ciphersuite=1;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='TLS_RSA_WITH_AES_128_CBC_SHA' WHERE ciphersuite=2;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='TLS_RSA_WITH_AES_256_CBC_SHA' WHERE ciphersuite=3;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='TLS_DHE_RSA_WITH_AES_128_CBC_SHA' WHERE ciphersuite=4;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='TLS_DHE_RSA_WITH_AES_256_CBC_SHA' WHERE ciphersuite=5;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='TLS_DHE_DSS_WITH_AES_128_CBC_SHA' WHERE ciphersuite=6;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='TLS_DHE_DSS_WITH_AES_256_CBC_SHA' WHERE ciphersuite=7;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='SSL_RSA_WITH_3DES_EDE_CBC_SHA' WHERE ciphersuite=8;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA' WHERE ciphersuite=9;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='SSL_DHE_DSS_WITH_3DES_EDE_CBC_SHA' WHERE ciphersuite=10;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='SSL_RSA_WITH_DES_CBC_SHA' WHERE ciphersuite=11;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='SSL_DHE_RSA_WITH_DES_CBC_SHA' WHERE ciphersuite=12;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='SSL_DHE_DSS_WITH_DES_CBC_SHA' WHERE ciphersuite=13;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='SSL_RSA_EXPORT_WITH_RC4_40_MD5' WHERE ciphersuite=14;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='SSL_RSA_EXPORT_WITH_DES40_CBC_SHA' WHERE ciphersuite=15;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='SSL_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA' WHERE ciphersuite=16;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='SSL_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA' WHERE ciphersuite=17;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='TLS_EMPTY_RENEGOTIATION_INFO_SCSV' WHERE ciphersuite=18;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='SSL_RSA_WITH_NULL_MD5' WHERE ciphersuite=19;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='SSL_RSA_WITH_NULL_SHA' WHERE ciphersuite=20;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='SSL_DH_anon_WITH_RC4_128_MD5' WHERE ciphersuite=21;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='TLS_DH_anon_WITH_AES_128_CBC_SHA' WHERE ciphersuite=22;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='TLS_DH_anon_WITH_AES_256_CBC_SHA' WHERE ciphersuite=23;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='SSL_DH_anon_WITH_3DES_EDE_CBC_SHA' WHERE ciphersuite=24;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='SSL_DH_anon_WITH_DES_CBC_SHA' WHERE ciphersuite=25;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='SSL_DH_anon_EXPORT_WITH_RC4_40_MD5' WHERE ciphersuite=26;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='SSL_DH_anon_EXPORT_WITH_DES40_CBC_SHA' WHERE ciphersuite=27;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='TLS_KRB5_WITH_RC4_128_SHA' WHERE ciphersuite=28;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='TLS_KRB5_WITH_RC4_128_MD5' WHERE ciphersuite=29;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='TLS_KRB5_WITH_3DES_EDE_CBC_SHA' WHERE ciphersuite=30;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='TLS_KRB5_WITH_3DES_EDE_CBC_MD5' WHERE ciphersuite=31;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='TLS_KRB5_WITH_DES_CBC_SHA' WHERE ciphersuite=32;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='TLS_KRB5_WITH_DES_CBC_MD5' WHERE ciphersuite=33;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='TLS_KRB5_EXPORT_WITH_RC4_40_SHA' WHERE ciphersuite=34;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='TLS_KRB5_EXPORT_WITH_RC4_40_MD5' WHERE ciphersuite=35;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA' WHERE ciphersuite=36;
UPDATE tls_simulator_ciphersuites SET new_ciphersuite='TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5' WHERE ciphersuite=37;
-- Drop the old column
ALTER TABLE tls_simulator_ciphersuites DROP COLUMN ciphersuite;
-- Rename the new column to match the entity declaration
ALTER TABLE tls_simulator_ciphersuites RENAME COLUMN new_ciphersuite TO ciphersuite ;

-- TLS Connection
-- Create a new column for migrating enum string values
ALTER TABLE tls_connection ADD COLUMN new_cipher_suite VARCHAR(128);
-- Migrate ordinal values to String in the new column
UPDATE tls_connection SET new_cipher_suite='SSL_RSA_WITH_RC4_128_MD5' WHERE cipher_suite=0;
UPDATE tls_connection SET new_cipher_suite='SSL_RSA_WITH_RC4_128_SHA' WHERE cipher_suite=1;
UPDATE tls_connection SET new_cipher_suite='TLS_RSA_WITH_AES_128_CBC_SHA' WHERE cipher_suite=2;
UPDATE tls_connection SET new_cipher_suite='TLS_RSA_WITH_AES_256_CBC_SHA' WHERE cipher_suite=3;
UPDATE tls_connection SET new_cipher_suite='TLS_DHE_RSA_WITH_AES_128_CBC_SHA' WHERE cipher_suite=4;
UPDATE tls_connection SET new_cipher_suite='TLS_DHE_RSA_WITH_AES_256_CBC_SHA' WHERE cipher_suite=5;
UPDATE tls_connection SET new_cipher_suite='TLS_DHE_DSS_WITH_AES_128_CBC_SHA' WHERE cipher_suite=6;
UPDATE tls_connection SET new_cipher_suite='TLS_DHE_DSS_WITH_AES_256_CBC_SHA' WHERE cipher_suite=7;
UPDATE tls_connection SET new_cipher_suite='SSL_RSA_WITH_3DES_EDE_CBC_SHA' WHERE cipher_suite=8;
UPDATE tls_connection SET new_cipher_suite='SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA' WHERE cipher_suite=9;
UPDATE tls_connection SET new_cipher_suite='SSL_DHE_DSS_WITH_3DES_EDE_CBC_SHA' WHERE cipher_suite=10;
UPDATE tls_connection SET new_cipher_suite='SSL_RSA_WITH_DES_CBC_SHA' WHERE cipher_suite=11;
UPDATE tls_connection SET new_cipher_suite='SSL_DHE_RSA_WITH_DES_CBC_SHA' WHERE cipher_suite=12;
UPDATE tls_connection SET new_cipher_suite='SSL_DHE_DSS_WITH_DES_CBC_SHA' WHERE cipher_suite=13;
UPDATE tls_connection SET new_cipher_suite='SSL_RSA_EXPORT_WITH_RC4_40_MD5' WHERE cipher_suite=14;
UPDATE tls_connection SET new_cipher_suite='SSL_RSA_EXPORT_WITH_DES40_CBC_SHA' WHERE cipher_suite=15;
UPDATE tls_connection SET new_cipher_suite='SSL_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA' WHERE cipher_suite=16;
UPDATE tls_connection SET new_cipher_suite='SSL_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA' WHERE cipher_suite=17;
UPDATE tls_connection SET new_cipher_suite='TLS_EMPTY_RENEGOTIATION_INFO_SCSV' WHERE cipher_suite=18;
UPDATE tls_connection SET new_cipher_suite='SSL_RSA_WITH_NULL_MD5' WHERE cipher_suite=19;
UPDATE tls_connection SET new_cipher_suite='SSL_RSA_WITH_NULL_SHA' WHERE cipher_suite=20;
UPDATE tls_connection SET new_cipher_suite='SSL_DH_anon_WITH_RC4_128_MD5' WHERE cipher_suite=21;
UPDATE tls_connection SET new_cipher_suite='TLS_DH_anon_WITH_AES_128_CBC_SHA' WHERE cipher_suite=22;
UPDATE tls_connection SET new_cipher_suite='TLS_DH_anon_WITH_AES_256_CBC_SHA' WHERE cipher_suite=23;
UPDATE tls_connection SET new_cipher_suite='SSL_DH_anon_WITH_3DES_EDE_CBC_SHA' WHERE cipher_suite=24;
UPDATE tls_connection SET new_cipher_suite='SSL_DH_anon_WITH_DES_CBC_SHA' WHERE cipher_suite=25;
UPDATE tls_connection SET new_cipher_suite='SSL_DH_anon_EXPORT_WITH_RC4_40_MD5' WHERE cipher_suite=26;
UPDATE tls_connection SET new_cipher_suite='SSL_DH_anon_EXPORT_WITH_DES40_CBC_SHA' WHERE cipher_suite=27;
UPDATE tls_connection SET new_cipher_suite='TLS_KRB5_WITH_RC4_128_SHA' WHERE cipher_suite=28;
UPDATE tls_connection SET new_cipher_suite='TLS_KRB5_WITH_RC4_128_MD5' WHERE cipher_suite=29;
UPDATE tls_connection SET new_cipher_suite='TLS_KRB5_WITH_3DES_EDE_CBC_SHA' WHERE cipher_suite=30;
UPDATE tls_connection SET new_cipher_suite='TLS_KRB5_WITH_3DES_EDE_CBC_MD5' WHERE cipher_suite=31;
UPDATE tls_connection SET new_cipher_suite='TLS_KRB5_WITH_DES_CBC_SHA' WHERE cipher_suite=32;
UPDATE tls_connection SET new_cipher_suite='TLS_KRB5_WITH_DES_CBC_MD5' WHERE cipher_suite=33;
UPDATE tls_connection SET new_cipher_suite='TLS_KRB5_EXPORT_WITH_RC4_40_SHA' WHERE cipher_suite=34;
UPDATE tls_connection SET new_cipher_suite='TLS_KRB5_EXPORT_WITH_RC4_40_MD5' WHERE cipher_suite=35;
UPDATE tls_connection SET new_cipher_suite='TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA' WHERE cipher_suite=36;
UPDATE tls_connection SET new_cipher_suite='TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5' WHERE cipher_suite=37;
-- Drop the old column
ALTER TABLE tls_connection DROP COLUMN cipher_suite;
-- Rename the new column to match the entity declaration
ALTER TABLE tls_connection RENAME COLUMN new_cipher_suite TO cipher_suite ;

-- TLS Test Data Set
-- Create a new column for migrating enum string values
ALTER TABLE tls_test_data_set_ciphersuites ADD COLUMN new_ciphersuite VARCHAR(128);
-- Migrate ordinal values to String in the new column
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='SSL_RSA_WITH_RC4_128_MD5' WHERE ciphersuite=0;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='SSL_RSA_WITH_RC4_128_SHA' WHERE ciphersuite=1;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='TLS_RSA_WITH_AES_128_CBC_SHA' WHERE ciphersuite=2;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='TLS_RSA_WITH_AES_256_CBC_SHA' WHERE ciphersuite=3;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='TLS_DHE_RSA_WITH_AES_128_CBC_SHA' WHERE ciphersuite=4;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='TLS_DHE_RSA_WITH_AES_256_CBC_SHA' WHERE ciphersuite=5;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='TLS_DHE_DSS_WITH_AES_128_CBC_SHA' WHERE ciphersuite=6;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='TLS_DHE_DSS_WITH_AES_256_CBC_SHA' WHERE ciphersuite=7;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='SSL_RSA_WITH_3DES_EDE_CBC_SHA' WHERE ciphersuite=8;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='SSL_DHE_RSA_WITH_3DES_EDE_CBC_SHA' WHERE ciphersuite=9;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='SSL_DHE_DSS_WITH_3DES_EDE_CBC_SHA' WHERE ciphersuite=10;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='SSL_RSA_WITH_DES_CBC_SHA' WHERE ciphersuite=11;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='SSL_DHE_RSA_WITH_DES_CBC_SHA' WHERE ciphersuite=12;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='SSL_DHE_DSS_WITH_DES_CBC_SHA' WHERE ciphersuite=13;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='SSL_RSA_EXPORT_WITH_RC4_40_MD5' WHERE ciphersuite=14;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='SSL_RSA_EXPORT_WITH_DES40_CBC_SHA' WHERE ciphersuite=15;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='SSL_DHE_RSA_EXPORT_WITH_DES40_CBC_SHA' WHERE ciphersuite=16;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='SSL_DHE_DSS_EXPORT_WITH_DES40_CBC_SHA' WHERE ciphersuite=17;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='TLS_EMPTY_RENEGOTIATION_INFO_SCSV' WHERE ciphersuite=18;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='SSL_RSA_WITH_NULL_MD5' WHERE ciphersuite=19;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='SSL_RSA_WITH_NULL_SHA' WHERE ciphersuite=20;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='SSL_DH_anon_WITH_RC4_128_MD5' WHERE ciphersuite=21;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='TLS_DH_anon_WITH_AES_128_CBC_SHA' WHERE ciphersuite=22;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='TLS_DH_anon_WITH_AES_256_CBC_SHA' WHERE ciphersuite=23;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='SSL_DH_anon_WITH_3DES_EDE_CBC_SHA' WHERE ciphersuite=24;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='SSL_DH_anon_WITH_DES_CBC_SHA' WHERE ciphersuite=25;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='SSL_DH_anon_EXPORT_WITH_RC4_40_MD5' WHERE ciphersuite=26;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='SSL_DH_anon_EXPORT_WITH_DES40_CBC_SHA' WHERE ciphersuite=27;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='TLS_KRB5_WITH_RC4_128_SHA' WHERE ciphersuite=28;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='TLS_KRB5_WITH_RC4_128_MD5' WHERE ciphersuite=29;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='TLS_KRB5_WITH_3DES_EDE_CBC_SHA' WHERE ciphersuite=30;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='TLS_KRB5_WITH_3DES_EDE_CBC_MD5' WHERE ciphersuite=31;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='TLS_KRB5_WITH_DES_CBC_SHA' WHERE ciphersuite=32;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='TLS_KRB5_WITH_DES_CBC_MD5' WHERE ciphersuite=33;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='TLS_KRB5_EXPORT_WITH_RC4_40_SHA' WHERE ciphersuite=34;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='TLS_KRB5_EXPORT_WITH_RC4_40_MD5' WHERE ciphersuite=35;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='TLS_KRB5_EXPORT_WITH_DES_CBC_40_SHA' WHERE ciphersuite=36;
UPDATE tls_test_data_set_ciphersuites SET new_ciphersuite='TLS_KRB5_EXPORT_WITH_DES_CBC_40_MD5' WHERE ciphersuite=37;
-- Drop the old column
ALTER TABLE tls_test_data_set_ciphersuites DROP COLUMN ciphersuite;
-- Rename the new column to match the entity declaration
ALTER TABLE tls_test_data_set_ciphersuites RENAME COLUMN new_ciphersuite TO ciphersuite ;

