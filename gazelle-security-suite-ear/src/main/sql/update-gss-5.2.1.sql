-- Clean tls_test_connection_data_item table. It is full of unused columns.
ALTER TABLE tls_test_connection_data_item DROP COLUMN data1 ;
ALTER TABLE tls_test_connection_data_item DROP COLUMN data2 ;
-- 'pemchain' and 'authtype' are unused column. But there is data in it.
-- transfer first 'pemchain' data into 'paim_chain' and 'authtype' data into 'auth_type'
-- then delete tables.
UPDATE tls_test_connection_data_item SET pem_chain = pemchain WHERE pemchain IS NOT NULL ;
UPDATE tls_test_connection_data_item SET auth_type = authtype WHERE authtype IS NOT NULL ;
ALTER TABLE tls_test_connection_data_item DROP COLUMN pemchain ;
ALTER TABLE tls_test_connection_data_item DROP COLUMN authtype ;

-- proxy 4.4.0 update
INSERT INTO app_configuration (id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'jms_communication_is_enabled', 'false');