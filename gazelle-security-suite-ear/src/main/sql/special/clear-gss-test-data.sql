\c gss

-- /!\ Certificates are not deleted by script to avoid destroying CAs. They must be deleted manually.

-- Clear TLS Connections and test instances
TRUNCATE TABLE tls_connection,tls_connection_data_item,tls_connection_data_item_aliases_item, tls_connection_tls_connection_data_item,tls_test_instance,tls_test_suite_instance_tls_test_instance;
SELECT setval('tls_connection_id_seq', 1);
SELECT setval('tls_connection_data_item_id_seq', 1);
SELECT setval('tls_connection_data_item_aliases_item_id_seq', 1);
SELECT setval('tls_test_instance_id_seq', 1);

-- Clear proxy messages
TRUNCATE TABLE pxy_connection, pxy_abstract_message, pxy_abstract_message_attributesforcommandset ;
SELECT setval('pxy_message_id_seq', 1);

-- Delete ATNA Questionnaires
DELETE FROM atna_audited_event;
DELETE FROM atna_network_communication;
DELETE FROM atna_tls_test;
DELETE FROM atna_questionnaire;
DELETE FROM atna_testing_session;

-- Rebuild ATNA Questionnaire template
INSERT INTO atna_testing_session (id, description, testing_session_id) VALUES (nextval('atna_testing_session_id_seq'), 'Demonstration', 1);
INSERT INTO atna_questionnaire (id, company, last_modified_date, last_modifier, local_user_authentication_process, non_network_mean_for_accessing_phi, oid, reviewer, status, system, system_type, username, testing_session_id) VALUES (nextval('atna_questionnaire_id_seq'), 'IHE-Europe', '2015-04-02 16:25:03.01', 'aberge', NULL, NULL, NULL, NULL, 0, 'TEMPLATE', 0, 'aberge', (select id from atna_testing_session where description = 'Demonstration'));
INSERT INTO atna_audited_event (id, audit_trail_message_url, audited_transaction, comment, issuing_actor, name, produced_by_system, validation_status, validator_oid, atna_questionnaire_id, file_location, validation_date, validation_log_path) VALUES (nextval('atna_audited_event_id_seq'), NULL, 'ITI-19 - Authenticate Node', '', 'Secure Node', 'Node-Authentication-failure', 0, 4, NULL, (select id from atna_questionnaire where system = 'TEMPLATE'), NULL, NULL, NULL);
INSERT INTO atna_audited_event (id, audit_trail_message_url, audited_transaction, comment, issuing_actor, name, produced_by_system, validation_status, validator_oid, atna_questionnaire_id, file_location, validation_date, validation_log_path) VALUES (nextval('atna_audited_event_id_seq'), NULL, 'ITI-46 - PIXV3 Update Notification', '', 'Patient Identity Cross-reference Manager', 'Patient-record-event', 0, 4, '1.3.6.1.4.1.12559.11.1.2.1.12.76', (select id from atna_questionnaire where system = 'TEMPLATE'), NULL, NULL, NULL);
INSERT INTO atna_audited_event (id, audit_trail_message_url, audited_transaction, comment, issuing_actor, name, produced_by_system, validation_status, validator_oid, atna_questionnaire_id, file_location, validation_date, validation_log_path) VALUES (nextval('atna_audited_event_id_seq'), NULL, 'ITI-43 - Retrieve Document Set', '', 'Document Consumer', 'PHI-import', 0, 4, '1.3.6.1.4.1.12559.11.1.2.1.12.22', (select id from atna_questionnaire where system = 'TEMPLATE'), NULL, NULL, NULL);
INSERT INTO atna_audited_event (id, audit_trail_message_url, audited_transaction, comment, issuing_actor, name, produced_by_system, validation_status, validator_oid, atna_questionnaire_id, file_location, validation_date, validation_log_path) VALUES (nextval('atna_audited_event_id_seq'), NULL, 'ITI-18 - Registry Stored Query', '', 'Document Consumer', 'Query Information', 0, 4, '1.3.6.1.4.1.12559.11.1.2.1.12.16', (select id from atna_questionnaire where system = 'TEMPLATE'), NULL, NULL, NULL);
INSERT INTO atna_network_communication (id, actor, comment, convey_phi, "inout", protected_by_tls, type, usage, atna_questionnaire_id) VALUES (nextval('atna_network_communication_id_seq'), 'Document Registry', '', 0, 0, 0, 2, 'ITI-62 - Delete Document Set', (select id from atna_questionnaire where system = 'TEMPLATE'));
INSERT INTO atna_network_communication (id, actor, comment, convey_phi, "inout", protected_by_tls, type, usage, atna_questionnaire_id) VALUES (nextval('atna_network_communication_id_seq'), 'Document Registry', '', 0, 0, 0, 2, 'ITI-57 - Update Document Set', (select id from atna_questionnaire where system = 'TEMPLATE'));
INSERT INTO atna_network_communication (id, actor, comment, convey_phi, "inout", protected_by_tls, type, usage, atna_questionnaire_id) VALUES (nextval('atna_network_communication_id_seq'), 'Document Registry', '', 0, 0, 0, 2, 'ITI-44 - Patient Identity Feed HL7 V3', (select id from atna_questionnaire where system = 'TEMPLATE'));
INSERT INTO atna_network_communication (id, actor, comment, convey_phi, "inout", protected_by_tls, type, usage, atna_questionnaire_id) VALUES (nextval('atna_network_communication_id_seq'), 'Document Registry', '', 0, 0, 0, 2, 'ITI-61 - Register On-Demand Document Entry', (select id from atna_questionnaire where system = 'TEMPLATE'));
INSERT INTO atna_network_communication (id, actor, comment, convey_phi, "inout", protected_by_tls, type, usage, atna_questionnaire_id) VALUES (nextval('atna_network_communication_id_seq'), 'Document Registry', '', 0, 0, 0, 2, 'ITI-42 - Register Document Set-b', (select id from atna_questionnaire where system = 'TEMPLATE'));
INSERT INTO atna_network_communication (id, actor, comment, convey_phi, "inout", protected_by_tls, type, usage, atna_questionnaire_id) VALUES (nextval('atna_network_communication_id_seq'), 'Document Registry', '', 0, 0, 0, 0, 'ITI-8 - Patient Identity Feed', (select id from atna_questionnaire where system = 'TEMPLATE'));
INSERT INTO atna_network_communication (id, actor, comment, convey_phi, "inout", protected_by_tls, type, usage, atna_questionnaire_id) VALUES (nextval('atna_network_communication_id_seq'), 'Patient Identity Cross-reference Manager', '', 0, 0, 0, 2, 'ITI-45 - PIXV3 Query', (select id from atna_questionnaire where system = 'TEMPLATE'));
INSERT INTO atna_network_communication (id, actor, comment, convey_phi, "inout", protected_by_tls, type, usage, atna_questionnaire_id) VALUES (nextval('atna_network_communication_id_seq'), 'Patient Identity Cross-reference Manager', '', 0, 0, 0, 2, 'ITI-44 - Patient Identity Feed HL7 V3', (select id from atna_questionnaire where system = 'TEMPLATE'));
INSERT INTO atna_network_communication (id, actor, comment, convey_phi, "inout", protected_by_tls, type, usage, atna_questionnaire_id) VALUES (nextval('atna_network_communication_id_seq'), 'Secure Node', '', 0, 0, 0, 4, 'ITI-1 - Maintain Time', (select id from atna_questionnaire where system = 'TEMPLATE'));
INSERT INTO atna_network_communication (id, actor, comment, convey_phi, "inout", protected_by_tls, type, usage, atna_questionnaire_id) VALUES (nextval('atna_network_communication_id_seq'), 'Document Consumer', '', 0, 1, 0, 2, 'ITI-43 - Retrieve Document Set', (select id from atna_questionnaire where system = 'TEMPLATE'));
INSERT INTO atna_network_communication (id, actor, comment, convey_phi, "inout", protected_by_tls, type, usage, atna_questionnaire_id) VALUES (nextval('atna_network_communication_id_seq'), 'Time Client', '', 0, 1, 0, 4, 'ITI-1 - Maintain Time', (select id from atna_questionnaire where system = 'TEMPLATE'));
INSERT INTO atna_network_communication (id, actor, comment, convey_phi, "inout", protected_by_tls, type, usage, atna_questionnaire_id) VALUES (nextval('atna_network_communication_id_seq'), 'Secure Node', '', 0, 1, 0, 1, 'ITI-20 - Record Audit Event', (select id from atna_questionnaire where system = 'TEMPLATE'));
INSERT INTO atna_network_communication (id, actor, comment, convey_phi, "inout", protected_by_tls, type, usage, atna_questionnaire_id) VALUES (nextval('atna_network_communication_id_seq'), 'Patient Identity Cross-reference Manager', '', 0, 1, 0, 2, 'ITI-46 - PIXV3 Update Notification', (select id from atna_questionnaire where system = 'TEMPLATE'));

-- Delete SyslogCollector data
TRUNCATE TABLE syslog_message ;
SELECT setval('syslog_message_id_seq', 1);
