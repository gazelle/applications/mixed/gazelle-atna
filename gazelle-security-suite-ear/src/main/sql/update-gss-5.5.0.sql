--
-- Name: xua_ancillary_transaction; Type: TABLE; Schema: public; Owner: gazelle; Tablespace:
--

CREATE TABLE xua_ancillary_transaction (
    id integer NOT NULL,
    description character varying(255),
    keyword character varying(255),
    name character varying(255),
    path_to_soap_body character varying(255),
    soap_action character varying(255)
);


ALTER TABLE public.xua_ancillary_transaction OWNER TO gazelle;

--
-- Name: xua_ancillary_transaction_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE xua_ancillary_transaction_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xua_ancillary_transaction_id_seq OWNER TO gazelle;

--
-- Name: xua_exception; Type: TABLE; Schema: public; Owner: gazelle; Tablespace:
--

CREATE TABLE xua_exception (
    id integer NOT NULL,
    exception_type integer,
    new_value character varying(255),
    xpath text,
    part_id integer
);


ALTER TABLE public.xua_exception OWNER TO gazelle;

--
-- Name: xua_exception_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE xua_exception_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xua_exception_id_seq OWNER TO gazelle;

--
-- Name: xua_message_variable; Type: TABLE; Schema: public; Owner: gazelle; Tablespace:
--

CREATE TABLE xua_message_variable (
    id integer NOT NULL,
    default_value character varying(255),
    token character varying(255),
    transaction_id integer
);


ALTER TABLE public.xua_message_variable OWNER TO gazelle;

--
-- Name: xua_message_variable_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE xua_message_variable_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xua_message_variable_id_seq OWNER TO gazelle;

--
-- Name: xua_service_provider_test_case; Type: TABLE; Schema: public; Owner: gazelle; Tablespace:
--

CREATE TABLE xua_service_provider_test_case (
    id integer NOT NULL,
    description text,
    expected_result character varying(255),
    keyword character varying(255),
    last_changed timestamp without time zone,
    last_modifier character varying(255)
);


ALTER TABLE public.xua_service_provider_test_case OWNER TO gazelle;

--
-- Name: xua_service_provider_test_case_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE xua_service_provider_test_case_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xua_service_provider_test_case_id_seq OWNER TO gazelle;

--
-- Name: xua_service_provider_test_instance; Type: TABLE; Schema: public; Owner: gazelle; Tablespace:
--

CREATE TABLE xua_service_provider_test_instance (
    id integer NOT NULL,
    request bytea,
    response bytea,
    test_status character varying(255),
    tested_endpoint character varying(255),
    "timestamp" timestamp without time zone,
    username character varying(255),
    test_case_id integer,
    transaction_id integer
);


ALTER TABLE public.xua_service_provider_test_instance OWNER TO gazelle;

--
-- Name: xua_service_provider_test_instance_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE xua_service_provider_test_instance_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xua_service_provider_test_instance_id_seq OWNER TO gazelle;

--
-- Name: xua_soap_header_part; Type: TABLE; Schema: public; Owner: gazelle; Tablespace:
--

CREATE TABLE xua_soap_header_part (
    dtype character varying(31) NOT NULL,
    id integer NOT NULL,
    created_offset integer,
    duration integer,
    local_name character varying(255),
    must_understand boolean,
    namespace_uri character varying(255),
    value character varying(255),
    password character varying(255),
    sts_endpoint character varying(255),
    username character varying(255),
    signed_element_namespace_uri character varying(255),
    signed_element_local_name character varying(255),
    test_case_id integer,
    keystore character varying(255),
    alias character varying(255),
    keystore_password character varying(255),
    private_key_password character varying(255)
);


ALTER TABLE public.xua_soap_header_part OWNER TO gazelle;

--
-- Name: xua_soap_header_part_id_seq; Type: SEQUENCE; Schema: public; Owner: gazelle
--

CREATE SEQUENCE xua_soap_header_part_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.xua_soap_header_part_id_seq OWNER TO gazelle;
--
-- Name: xua_ancillary_transaction_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace:
--

ALTER TABLE ONLY xua_ancillary_transaction
    ADD CONSTRAINT xua_ancillary_transaction_pkey PRIMARY KEY (id);


--
-- Name: xua_exception_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace:
--

ALTER TABLE ONLY xua_exception
    ADD CONSTRAINT xua_exception_pkey PRIMARY KEY (id);


--
-- Name: xua_message_variable_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace:
--

ALTER TABLE ONLY xua_message_variable
    ADD CONSTRAINT xua_message_variable_pkey PRIMARY KEY (id);


--
-- Name: xua_service_provider_test_case_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace:
--

ALTER TABLE ONLY xua_service_provider_test_case
    ADD CONSTRAINT xua_service_provider_test_case_pkey PRIMARY KEY (id);


--
-- Name: xua_service_provider_test_instance_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace:
--

ALTER TABLE ONLY xua_service_provider_test_instance
    ADD CONSTRAINT xua_service_provider_test_instance_pkey PRIMARY KEY (id);


--
-- Name: xua_soap_header_part_pkey; Type: CONSTRAINT; Schema: public; Owner: gazelle; Tablespace:
--

ALTER TABLE ONLY xua_soap_header_part
    ADD CONSTRAINT xua_soap_header_part_pkey PRIMARY KEY (id);

--
-- Name: fk_2p4ij7mxf9e0lr0tfbsbp20vw; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY xua_service_provider_test_instance
    ADD CONSTRAINT fk_2p4ij7mxf9e0lr0tfbsbp20vw FOREIGN KEY (transaction_id) REFERENCES xua_ancillary_transaction(id);


--
-- Name: fk_a358pmfdujmer38h7k1xpn96f; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY xua_soap_header_part
    ADD CONSTRAINT fk_a358pmfdujmer38h7k1xpn96f FOREIGN KEY (test_case_id) REFERENCES xua_service_provider_test_case(id);


--
-- Name: fk_bfkcbkxlqwxyh6pmqkbt8nvxk; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY xua_service_provider_test_instance
    ADD CONSTRAINT fk_bfkcbkxlqwxyh6pmqkbt8nvxk FOREIGN KEY (test_case_id) REFERENCES xua_service_provider_test_case(id);


--
-- Name: fk_hrl8prtbpjgm7us84svlisc6v; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY xua_message_variable
    ADD CONSTRAINT fk_hrl8prtbpjgm7us84svlisc6v FOREIGN KEY (transaction_id) REFERENCES xua_ancillary_transaction(id);


--
-- Name: fk_t3b4kiyy8v7e7ymgm48vi0i5m; Type: FK CONSTRAINT; Schema: public; Owner: gazelle
--

ALTER TABLE ONLY xua_exception
    ADD CONSTRAINT fk_t3b4kiyy8v7e7ymgm48vi0i5m FOREIGN KEY (part_id) REFERENCES xua_soap_header_part(id);

-- New application preferences
INSERT INTO app_configuration(id, variable, value) VALUES (nextval('app_configuration_id_seq'), 'xua_soap_body_directory', '/opt/atna/xuatesting/');