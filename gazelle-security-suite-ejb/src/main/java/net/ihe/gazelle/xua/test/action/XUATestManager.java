package net.ihe.gazelle.xua.test.action;

import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.xua.model.ServiceProviderTestCase;
import net.ihe.gazelle.xua.model.ServiceProviderTestCaseQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import net.ihe.gazelle.common.filter.Filter;

import java.io.Serializable;

/**
 * Created by aberge on 02/06/17.
 */
@Name("xuaTestManager")
@Scope(ScopeType.PAGE)
public class XUATestManager implements Serializable, UserAttributeCommon {

    private Filter<ServiceProviderTestCase> filter;

    @In(value="gumUserService")
    private UserService userService;

    public Filter<ServiceProviderTestCase> getFilter() {
        if (filter == null){
            filter = new Filter<ServiceProviderTestCase>(getHqlCriteria());
        }
        return filter;
    }

    private HQLCriterionsForFilter<ServiceProviderTestCase> getHqlCriteria() {
        ServiceProviderTestCaseQuery query = new ServiceProviderTestCaseQuery();
        HQLCriterionsForFilter<ServiceProviderTestCase> criteria = query.getHQLCriterionsForFilter();
        return criteria;
    }

    public FilterDataModel<ServiceProviderTestCase> getTestCases(){
        return new FilterDataModel<ServiceProviderTestCase>(getFilter()) {
            @Override
            protected Object getId(ServiceProviderTestCase serviceProviderTestCase) {
                return serviceProviderTestCase.getId();
            }
        };
    }

    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }

    public void clearFilter(){
        getFilter().clear();
    }


    public String editTest(ServiceProviderTestCase test){
        // TODO
        return null;
    }

    public void deleteTest(ServiceProviderTestCase test){
        // TODO
    }

    public String executeTestCase(ServiceProviderTestCase test){
        return "/xuatesting/execute.seam?testcaseid=" + test.getId();
    }
}
