package net.ihe.gazelle.xua.test.action;

import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.common.util.XmlFormatter;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.xua.dao.AncillaryTransactionDAO;
import net.ihe.gazelle.xua.model.AncillaryTransaction;
import org.apache.poi.util.IOUtils;
import org.dom4j.DocumentException;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;

import javax.faces.context.FacesContext;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.util.Map;

/**
 * Created by aberge on 08/06/17.
 */
@Name("ancillaryTransactionDisplay")
@Scope(ScopeType.PAGE)
public class AncillaryTransactionDisplay implements Serializable {

    private AncillaryTransaction selectedTransaction;

    @Create
    public void initialize() {
        Map<String, String> urlParams = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String idAsString = urlParams.get("id");
        if (idAsString == null) {
            selectedTransaction = null;
        } else {
            try {
                Integer id = Integer.parseInt(idAsString);
                selectedTransaction = AncillaryTransactionDAO.getTransactionById(id);
            } catch (NumberFormatException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, idAsString + " is not a valid transaction id");
            }
        }
    }


    public AncillaryTransaction getSelectedTransaction() {
        return selectedTransaction;
    }

    public String getSoapBodyContent() {
        if (this.selectedTransaction != null && this.selectedTransaction.getPathToSoapBody() != null) {
            String directoryPath = PreferenceService.getString("xua_soap_body_directory");
            if (directoryPath == null) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR, "The system is not able to retrieve the SOAP Body, xua_soap_body_directory is missing");
                return null;
            } else {
                String fileContentAsString = null;
                try {
                    File bodyFile = new File(directoryPath, selectedTransaction.getPathToSoapBody());
                    if (bodyFile.exists()) {
                        byte[] fileContent = IOUtils.toByteArray(new FileInputStream(bodyFile));
                        if (fileContent != null) {
                            fileContentAsString = new String(fileContent, Charset.forName("UTF-8"));
                            return XmlFormatter.format(fileContentAsString);
                        } else {
                            FacesMessages.instance().add("Referenced file is empty");
                            return null;
                        }
                    } else {
                        FacesMessages.instance().add(StatusMessage.Severity.ERROR, "File " + selectedTransaction.getPathToSoapBody() + " does not exist in " + directoryPath);
                        return null;
                    }
                } catch (IOException e) {
                    FacesMessages.instance().add("System was not able to retrieve file " + selectedTransaction.getPathToSoapBody() + "in " + directoryPath);
                    return null;
                } catch (DocumentException e) {
                    return fileContentAsString;
                }
            }
        } else {
            return null;
        }
    }

    public String backToTransactionList(){
        return Pages.XUA_TRANSACTIONS.getMenuLink();
    }
}
