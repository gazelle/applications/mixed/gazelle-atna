package net.ihe.gazelle.atna.tls.test;

import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;

import net.ihe.gazelle.atna.action.ApplicationManagerBean;
import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestDataSet;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestDataSetQuery;

import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

@Name("dataSetListManager")
@Scope(ScopeType.PAGE)
public class DataSetListManager implements TlsTestDataSetDisplayer, UserAttributeCommon {

    private static final long serialVersionUID = 3835894246842009250L;

    private int pendingDataSetId = 0;

    private transient Filter<TlsTestDataSet> filter;
    private transient FilterDataModel<TlsTestDataSet> dataSets;

    @In(value="gumUserService")
    private UserService userService;

    public int getPendingDataSetId() {
        return pendingDataSetId;
    }

    public void setPendingDataSetId(int id) {
        this.pendingDataSetId = id;
    }

    public Filter<TlsTestDataSet> getFilter() {
        if (filter == null) {
            filter = new Filter<TlsTestDataSet>(getHQLCriterionsForFilter(), FacesContext.getCurrentInstance().getExternalContext()
                    .getRequestParameterMap());
        }
        return filter;
    }

    public void setFilter(Filter<TlsTestDataSet> filter) {
        this.filter = filter;
    }

    public FilterDataModel<TlsTestDataSet> getDataSets() {
        if (dataSets == null) {
            dataSets = new FilterDataModel<TlsTestDataSet>(getFilter()) {
                @Override
                protected Object getId(TlsTestDataSet t) {
                    // TODO Auto-generated method stub
                    return t.getId();
                }
            };
        }
        return dataSets;
    }

    public void setDataSets(FilterDataModel<TlsTestDataSet> dataSets) {
        this.dataSets = dataSets;
    }

    public void resetFilter() {
        getFilter().clear();
        getDataSets().resetCache();
    }

    public String navigateEditDataSet(int dataSetId) {
        return Pages.TLS_CREATE_DATA_SET.getMenuLink() + "?dataSet=" + dataSetId;
    }

    public String navigateNewDataSet() {
        return Pages.TLS_CREATE_DATA_SET.getMenuLink();
    }

    public String linkToEditDataSet(int dataSetId) {
        return ApplicationManagerBean.getUrl() + navigateEditDataSet(dataSetId);
    }

    public TlsTestDataSet getPendingDataSet() {
        if (pendingDataSetId != 0) {
            TlsTestDataSet currentDataSet;
            EntityManager entityManager = EntityManagerService.provideEntityManager();
            currentDataSet = entityManager.find(TlsTestDataSet.class, pendingDataSetId);
            return currentDataSet;
        }
        return null;
    }

    public String getPendingDataSetName() {
        TlsTestDataSet currentDataSet = getPendingDataSet();
        if (currentDataSet != null) {
            return currentDataSet.getName();
        }
        return null;
    }

    @Override
    public String linkToCertificate() {
        TlsTestDataSet currentDataSet = getPendingDataSet();
        if (currentDataSet != null && currentDataSet.getCertificate() != null) {
            return ApplicationManagerBean.getUrl() + Pages.PKI_VIEW_CERTIFICATE.getMenuLink() +
                    "?id=" + currentDataSet.getCertificate().getId();
        }
        return null;
    }

    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }

    public void deleteDataSet() {
        //TODO implement deletion of the data set of the database.
        //pendingDataSet must be deleted.
    }

    private HQLCriterionsForFilter<TlsTestDataSet> getHQLCriterionsForFilter() {
        TlsTestDataSetQuery query = new TlsTestDataSetQuery();
        HQLCriterionsForFilter<TlsTestDataSet> criterions = query.getHQLCriterionsForFilter();
        criterions.addPath("dataSetName", query.name());
        criterions.addPath("context", query.context());
        return criterions;
    }
}
