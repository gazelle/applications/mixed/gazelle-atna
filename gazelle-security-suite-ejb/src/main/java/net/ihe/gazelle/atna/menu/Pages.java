package net.ihe.gazelle.atna.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.common.pages.Page;

public enum Pages implements Page {

    CAS_HOME("/cas/home.seam", "fa-sign-in", "net.ihe.gazelle.atna.Login", Authorizations.ALL),

    HOME("/home.seam", "fa-home", "net.ihe.gazelle.atna.Home", Authorizations.ALL),

    ERROR("/error.seam", null, null, Authorizations.ALL),

    ERROR_EXPIRED("/errorExpired.seam", null, null, Authorizations.ALL),

    PKI_TOP("", "fa-certificate", "net.ihe.gazelle.atna.PKI", Authorizations.ALL, Authorizations.PKI),

    PKI_VIEW_CERTIFICATE("/certificate/view.seam", null, null, Authorizations.ALL),

    PKI_VIEW_REQUEST("/request/view.seam", null, null, Authorizations.LOGGED, Authorizations.PKI),

    PKI_OLD_VIEW_CERTIFICATE("/details/cert.seam", null, null, Authorizations.ALL),

    PKI_LIST_CERTIFICATES("/certificate/list.seam", "fa-certificate", "net.ihe.gazelle.atna.ListCertificates", Authorizations.ALL, Authorizations.PKI),

    PKI_LIST_REQUESTS("/request/list.seam", "fa-comments-o", "net.ihe.gazelle.atna.ListCertificateRequests", Authorizations.LOGGED, Authorizations.PKI),

    PKI_VALIDATE("/validate.seam", "fa-check-square-o", "net.ihe.gazelle.atna.CertificateValidation", Authorizations.PKI),

    PKI_REQUEST_CERTIFICATE("/request/withoutcsr.seam", "fa-comment-o", "net.ihe.gazelle.atna.RequestACertificate", Authorizations.LOGGED, Authorizations.PKI),

    PKI_SUBMIT_CSR("/request/withcsr.seam", "fa-comment-o", "net.ihe.gazelle.atna.SubmitCSR", Authorizations.LOGGED, Authorizations.PKI),

    PKI_INSTALL_AUTOLOGIN("/request/autologin.seam", "fa-globe", "net.ihe.gazelle.atna.InstallBrowserCertificateForCASAutologin", Authorizations.LOGGED, Authorizations.PKI),

    PKI_DOWNLOAD_CA("/certificate/downloadcacert.seam", null, null, Authorizations.LOGGED, Authorizations.PKI),

    PKI_CREATE_FROM_CSV("/certificate/createfromcsv.seam", "fa-certificate", "Create certificates from CSV", Authorizations.ADMIN, Authorizations.PKI),

    PKI_CREATE_CA("/request/ca.seam", "fa-certificate", "net.ihe.gazelle.atna.CreateACertificateAuthority", Authorizations.ADMIN, Authorizations.PKI),

    PKI_IMPORT_P12("/certificate/import/p12.seam", "fa-upload", "net.ihe.gazelle.atna.ImportP12", Authorizations.ADMIN, Authorizations.PKI),

    PKI_IMPORT_PEM("/certificate/import/pem.seam", "fa-upload", "net.ihe.gazelle.atna.ImportPEM", Authorizations.ADMIN, Authorizations.PKI),

    SYSLOG_ADMIN("/syslog/administration.seam", "fa-history", "Syslog Collector administration", Authorizations.ADMIN, Authorizations.SYSLOG),

    SYSLOG_LIST("/syslog/list.seam", "fa-history", "Syslog Collector", Authorizations.ALL, Authorizations.SYSLOG),

    SYSLOG_VIEW("/syslog/view.seam", null, null, Authorizations.ALL, Authorizations.SYSLOG),

    TLS_CREATE_CERTIFICATE("/certificate/custom/create.seam", "fa-flask", "net.ihe.gazelle.atna.CreateACustomCertificate", Authorizations.ADMIN, Authorizations.TLS),

    TLS_CREATE_CA("/certificate/custom/createca.seam", "fa-flask", "net.ihe.gazelle.atna.CreateACustomCertificateAuthority", Authorizations.ADMIN, Authorizations.TLS),

    TLS_CREATE_CERTIFICATE_SET("/certificate/custom/generateset.seam", "fa-flask", "net.ihe.gazelle.atna.GenerateASetOfCustomCertificates", Authorizations.ADMIN, Authorizations.TLS),

    TLS_CREATE_DATA_SET("/dataset/create.seam", "fa-database", "net.ihe.gazelle.atna.CreateATestDataSet", Authorizations.ADMIN, Authorizations.TLS),

    TLS_CREATE_TEST_CASE("/testcase/create.seam", "fa-flask", "net.ihe.gazelle.atna.CreateATestCase", Authorizations.ADMIN, Authorizations.TLS),

    TLS_CREATE_TEST_SUITE("/testsuite/create.seam", "fa-flask", "net.ihe.gazelle.atna.CreateATestSuite", Authorizations.ADMIN, Authorizations.TLS),

    TLS_EDIT_CLIENT("/client/edit.seam", "fa-cog", "net.ihe.gazelle.atna.CreateTLSSSLClientSimulator", Authorizations.ADMIN, Authorizations.TLS),

    TLS_EDIT_SERVER("/server/edit.seam", "fa-cog", "net.ihe.gazelle.atna.CreateTLSSSLServerSimulator", Authorizations.ADMIN, Authorizations.TLS),

    TLS_LIST_CERTIFICATES("/certificate/custom/list.seam", "fa-certificate", "net.ihe.gazelle.atna.CustomCertificates", Authorizations.LOGGED, Authorizations.TLS),

    TLS_LIST_CLIENTS("/client/listClients.seam", "fa-cogs", "net.ihe.gazelle.atna.Clients", Authorizations.ALL, Authorizations.TLS),

    TLS_LIST_SERVER("/server/listServers.seam", "fa-cogs", "net.ihe.gazelle.atna.Servers", Authorizations.ALL, Authorizations.TLS),

    TLS_LIST_CONNECTIONS("/simulators/logs.seam", "fa-search", "net.ihe.gazelle.atna.ConnectionLogs", Authorizations.ALL, Authorizations.TLS),

    TLS_LIST_DATA_SET("/dataset/list.seam", "fa-database", "net.ihe.gazelle.atna.DataSets", Authorizations.LOGGED, Authorizations.TLS),

    TLS_LIST_TEST_CASE("/testcase/list.seam", "fa-flask", "net.ihe.gazelle.atna.TestCases", Authorizations.LOGGED, Authorizations.TLS),

    TLS_LIST_TEST_SUITE("/testsuite/list.seam", "fa-flask", "net.ihe.gazelle.atna.TestSuites", Authorizations.LOGGED, Authorizations.TLS),

    TLS_PROXY_DICOM("/messages/dicom.seam", null, null, Authorizations.ALL, Authorizations.TLS),

    TLS_PROXY_HL7("/messages/hl7.seam", null, null, Authorizations.ALL, Authorizations.TLS),

    TLS_PROXY_HTTP("/messages/http.seam", null, null, Authorizations.ALL, Authorizations.TLS),

    TLS_PROXY_RAW("/messages/raw.seam", null, null, Authorizations.ALL, Authorizations.TLS),

    TLS_PROXY_SYSLOG("/messages/syslog.seam", null, null, Authorizations.ALL, Authorizations.TLS),

    TLS_PROXY_MESSAGES("/messages.seam", null, null, Authorizations.ALL),

    TLS_RUN_TEST_CASE("/testcase/play.seam", null, null, Authorizations.LOGGED, Authorizations.TLS),

    TLS_RUN_TEST_SUITE("/testsuite/play.seam", null, null, Authorizations.LOGGED, Authorizations.TLS),

    TLS_SIMULATOR_TOP("", "fa-cogs", "net.ihe.gazelle.atna.Simulators", Authorizations.ALL, Authorizations.TLS),

    TLS_TESTING_TOP("", "fa-flask", "net.ihe.gazelle.atna.Testing", Authorizations.LOGGED, Authorizations.TLS),

    TLS_TEST_CLIENT("/client/test.seam", null, null, Authorizations.ALL, Authorizations.TLS),

    TLS_TEST_SERVER("/server/test.seam", null, null, Authorizations.ALL, Authorizations.TLS),

    TLS_TOP("", "fa-lock", "net.ihe.gazelle.atna.TLSSSL", Authorizations.ALL, Authorizations.TLS),

    TLS_VIEW_TEST_CASE("/testcase/view.seam", null, null, Authorizations.LOGGED, Authorizations.TLS),

    TLS_VIEW_TEST_SUITE("/testsuite/view.seam", null, null, Authorizations.LOGGED, Authorizations.TLS),

    TLS_VIEW_TEST_INSTANCE("/testinstance/view.seam", null, null, Authorizations.ALL, Authorizations.TLS),

    TLS_VIEW_CONNECTION("/details/connection.seam", null, null, Authorizations.ALL, Authorizations.TLS),

    XUA_TOP("", "fa-key", "net.ihe.gazelle.atna.XUA", Authorizations.ALL, Authorizations.XUA),

    XUA_CLASS_DOC("/docum/classes/listClasses.seam", "fa-book", "net.ihe.gazelle.atna.ClassDocumentation", Authorizations.ALL, Authorizations.XUA),

    XUA_CONSTRAINT_DOC("/docum/constraints/constraints.seam", "fa-book", "net.ihe.gazelle.atna.ConstraintDocumentation", Authorizations.ALL, Authorizations.XUA),

    XUA_CONSTRAINT_ADMIN("/docum/upload/uploadConstraints.seam", "fa-wrench", "net.ihe.gazelle.atna.ConstraintAdministration", Authorizations.ADMIN, Authorizations.XUA),

    XUA_SERVICE_PROVIDER_TESTS("", "fa-flask", "net.ihe.gazelle.atna.XUAProviderTests", Authorizations.LOGGED, Authorizations.XUA),

    XUA_TEST_CASES("/xuatesting/testcases.seam", "", "net.ihe.gazelle.atna.XUATestCases", Authorizations.LOGGED, Authorizations.XUA),
    XUA_TEST_CASE_DETAIL("/xuatesting/testCaseDetails.seam", null, null, Authorizations.LOGGED, Authorizations.XUA),

    XUA_TEST_INSTANCES("/xuatesting/testinstances.seam", "", "net.ihe.gazelle.atna.XUATestInstances", Authorizations.LOGGED, Authorizations.XUA),
    XUA_TEST_INSTANCES_DETAILS("/xuatesting/testInstanceDetails.seam", null, null, Authorizations.LOGGED, Authorizations.XUA),

    XUA_TRANSACTIONS("/xuatesting/admin/transactions.seam", "", "net.ihe.gazelle.atna.AncillaryTransactions", Authorizations.ADMIN, Authorizations.XUA),
    XUA_EXECUTE_TEST("/xuatesting/execute.seam", null, null, Authorizations.LOGGED, Authorizations.XUA),
    XUA_TRANSACTION_DETAILS("/xuatesting/transactionDetails.seam", null, null, Authorizations.LOGGED, Authorizations.XUA),

    ADMIN_PREFERENCES("/admin/configure.seam", "fa-wrench", "net.ihe.gazelle.atna.ConfigureApplicationPreferences", Authorizations.ADMIN),

    ADMIN_TOP("", null, "net.ihe.gazelle.atna.Administration", Authorizations.ADMIN);

    private String link;

    private Authorization[] authorizations;

    private String label;

    private String icon;

    Pages(String link, String icon, String label, Authorization... authorizations) {
        this.link = link;
        this.authorizations = authorizations;
        this.label = label;
        this.icon = icon;
    }

    @Override
    public String getId() {
        return name();
    }

    /*
     * Get page link with '.seam' extension
     */
    @Override
    public String getMenuLink() {
        return link;
    }

    /*
     * Get page link with '.xhtml' extension
     */
    @Override
    public String getLink() {
        return link.replace(".seam", ".xhtml");
    }

    @Override
    public String getIcon() {
        return icon;
    }

    @Override
    public Authorization[] getAuthorizations() {
        return authorizations;
    }

    @Override
    public String getLabel() {
        return label;
    }

}
