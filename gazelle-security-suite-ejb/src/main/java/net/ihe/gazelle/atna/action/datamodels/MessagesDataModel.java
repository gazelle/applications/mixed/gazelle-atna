package net.ihe.gazelle.atna.action.datamodels;

import javax.faces.context.FacesContext;

import net.ihe.gazelle.common.filter.HibernateDataModel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.HQLRestriction;
import net.ihe.gazelle.proxy.model.message.AbstractMessage;
import net.ihe.gazelle.proxy.model.message.AbstractMessageQuery;

public class MessagesDataModel extends HibernateDataModel<AbstractMessage> {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private HQLRestriction connectionRestriction;

    public MessagesDataModel(String uuid) {
        super(AbstractMessage.class);
        connectionRestriction = new AbstractMessageQuery().connection().uuid().eqRestriction(uuid);
    }

    @Override
    protected void appendFilters(FacesContext context, HQLQueryBuilder<AbstractMessage> queryBuilder) {
        super.appendFilters(context, queryBuilder);
        queryBuilder.addRestriction(connectionRestriction);
    }

    @Override
    protected Object getId(AbstractMessage t) {
        // TODO Auto-generated method stub
        return t.getId();
    }
}
