package net.ihe.gazelle.atna.tls.test.testsuite;

import javax.faces.context.FacesContext;

import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.FilterDataModel;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestSuite;

public abstract class TestSuiteBrowser {

    private Filter<TlsTestSuite> filter;
    private FilterDataModel<TlsTestSuite> testSuites;
    private TlsTestSuite pendingTestSuite;

    public TestSuiteBrowser() {
        super();
    }

    public Filter<TlsTestSuite> getFilter() {
        if (filter == null) {
            filter = new Filter<TlsTestSuite>(getHQLCriterionsForFilter(),
                    FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap());
        }
        return filter;
    }

    public abstract HQLCriterionsForFilter<TlsTestSuite> getHQLCriterionsForFilter();

    public FilterDataModel<TlsTestSuite> getTestSuites() {
        if (testSuites == null) {
            testSuites = new FilterDataModel<TlsTestSuite>((Filter<TlsTestSuite>) getFilter()) {
                @Override
                protected Object getId(TlsTestSuite t) {
                    // TODO Auto-generated method stub
                    return t.getId();
                }
            };
        }
        return testSuites;
    }

    public TlsTestSuite getPendingTestSuite() {
        return pendingTestSuite;
    }

    public void setPendingTestSuite(TlsTestSuite pendingInstance) {
        this.pendingTestSuite = pendingInstance;
    }

    public void reset() {
        getFilter().clear();
        getTestSuites().resetCache();
    }

    public String getPermanentLink(TlsTestSuite testSuite) {
        return Pages.TLS_VIEW_TEST_SUITE.getMenuLink() + "?id=" + testSuite.getId();
    }

}