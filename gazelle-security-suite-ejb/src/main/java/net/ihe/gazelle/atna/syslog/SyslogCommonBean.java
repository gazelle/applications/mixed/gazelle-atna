package net.ihe.gazelle.atna.syslog;

import net.ihe.gazelle.common.util.DateDisplayUtil;
import net.ihe.gazelle.syslog.model.SyslogCollectorMessage;
import org.jboss.seam.international.LocaleSelector;

import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by cel on 07/09/16.
 */
public abstract class SyslogCommonBean {

    private Locale locale = null;
    private TimeZone timeZone = null;

    public String getSyslogMessageTimestamp(SyslogCollectorMessage message) {
        return message.getTimestamp(getTimeZone(), getLocale());
    }

    private Locale getLocale() {
        if (locale == null) {
            locale = LocaleSelector.instance().getLocale();
        }
        return locale;
    }

    private TimeZone getTimeZone() {
        if (timeZone == null) {
            timeZone = DateDisplayUtil.getTimeZone();
        }
        return timeZone;
    }
}
