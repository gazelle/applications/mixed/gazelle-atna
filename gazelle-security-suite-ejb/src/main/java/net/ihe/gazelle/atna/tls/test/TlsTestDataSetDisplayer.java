package net.ihe.gazelle.atna.tls.test;

/**
 * Every page that includes /dataset/viewPanel.xhtml must use a bean that implements this interface.
 *
 * Created by cel on 08/10/15.
 */
public interface TlsTestDataSetDisplayer {

    public String linkToCertificate();

}