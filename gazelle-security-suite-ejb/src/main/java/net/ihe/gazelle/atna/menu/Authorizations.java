package net.ihe.gazelle.atna.menu;

import net.ihe.gazelle.common.pages.Authorization;
import net.ihe.gazelle.preferences.PreferenceService;
import org.jboss.seam.security.Identity;

public enum Authorizations implements Authorization {

    ALL,
    LOGGED,
    ADMIN,
    MONITOR,
    PKI,
    TLS,
    XUA,
    ATNA,
    SYSLOG,
    TESTEDITOR;

    @Override
    public boolean isGranted(Object... context) {
        switch (this) {
            case ALL:
                return true;
            case LOGGED:
                return Identity.instance().isLoggedIn();
            case ADMIN:
                return Identity.instance().hasRole("admin_role");
            case MONITOR:
                return Identity.instance().hasRole("monitor_role");
            case PKI:
                return isPki();
            case TLS:
                return isTls();
            case ATNA:
                return isAtna() ;
            case XUA:
                return isXua();
            case SYSLOG:
                return isSyslog();
            case TESTEDITOR:
                return Identity.instance().hasRole("admin_role") || Identity.instance().hasRole("tests_editor_role");
            default:
                return false;
        }
    }

    private boolean isPki() {
        return isEnabled("pki_mode_enabled");
    }

    private boolean isTls() {
        return isEnabled("tls_mode_enabled");
    }

    private boolean isAtna() {
        return isEnabled("atna_mode_enabled");
    }

    private boolean isXua() {
        return isEnabled("xua_mode_enabled");
    }

    private boolean isSyslog() {
        return isEnabled("syslog_collector_enabled");
    }

    private boolean isEnabled(String preferenceMode) {
        Boolean is = PreferenceService.getBoolean(preferenceMode);
        return is != null && is;
    }

}
