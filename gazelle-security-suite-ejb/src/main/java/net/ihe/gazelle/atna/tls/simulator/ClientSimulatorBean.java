package net.ihe.gazelle.atna.tls.simulator;

import net.ihe.gazelle.atna.action.ApplicationManagerBean;
import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.pki.validator.CertificateValidatorType;
import net.ihe.gazelle.simulators.tls.model.TlsClient;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by cel on 18/09/15.
 */
@Name("clientSimulatorBean")
@Scope(ScopeType.PAGE)
public class ClientSimulatorBean extends AbstractSimulatorBean<TlsClient> {

    private static final long serialVersionUID = 3144424385761746426L;
    private static Logger log = LoggerFactory.getLogger(ClientSimulatorBean.class);

    @Override
    @Create
    public void init() {
        super.init();
        // If it's a simulator creation
        if (!isEditMode()) {
            setCertificateValidator(CertificateValidatorType.TLS_SERVER);
        }
    }

    @Override
    public TlsClient newSimulatorInstance() {
        return new TlsClient();
    }

    @Override
    public Class<TlsClient> getSimulatorClass() {
        return TlsClient.class;
    }

    @Override
    public String getSimulatorDescription() {
        return "TLS Client";
    }

    @Override
    public String linkToSimulatorList() {
        return ApplicationManagerBean.getUrl() + Pages.TLS_LIST_CLIENTS.getMenuLink();
    }

    @Override
    public String navigateToSimulatorView(Integer id) {
        return Pages.TLS_TEST_CLIENT.getMenuLink() + "?simulator=" + id;
    }

    @Override
    public boolean isPeerAuthenticationRequired() {
        return true;
    }

}
