package net.ihe.gazelle.atna.action.converters;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import net.ihe.gazelle.simulators.tls.enums.SimulatorType;

import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.faces.Converter;
import org.jboss.seam.annotations.intercept.BypassInterceptors;

@Name("simulatorTypeConverter")
@BypassInterceptors
@Converter(forClass = SimulatorType.class)
public class SimulatorTypeConverter implements javax.faces.convert.Converter {

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        SimulatorType[] values = SimulatorType.values();
        for (SimulatorType simulatorType : values) {
            if (simulatorType.toString().equals(value)) {
                return simulatorType;
            }
        }
        return null;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        return value.toString();
    }

}
