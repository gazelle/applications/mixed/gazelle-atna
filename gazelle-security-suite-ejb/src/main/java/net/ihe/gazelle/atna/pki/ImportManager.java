package net.ihe.gazelle.atna.pki;

import net.ihe.gazelle.pki.CertificateDAO;
import net.ihe.gazelle.pki.CertificateManager;
import net.ihe.gazelle.pki.PKIXImporter;
import net.ihe.gazelle.pki.bouncycastle.CertificateBC;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.PKiProvider;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.richfaces.event.FileUploadEvent;
import org.richfaces.model.UploadedFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.X509Certificate;
import java.util.Enumeration;

@Name("importManager")
@Scope(ScopeType.PAGE)
public class ImportManager implements Serializable {

    private static final long serialVersionUID = 1338892573529018410L;
    private static Logger log = LoggerFactory.getLogger(ImportManager.class);
    private byte[] data;
    private byte[] pkdata;
    private String password;
    private String pkpassword;
    @In
    private transient EntityManager entityManager;

    //protected PKiProvider mo_pKiProvider =  (PKiProvider) org.jboss.seam.Component.getInstance("CertificateBC", true);
    protected PKiProvider mo_pKiProvider =  new CertificateBC();

    public ImportManager() {
        super();
    }

    /**
     * Getter of attribute mo_pkiProvider.
     * @return the Instance of PKIProvider.
     */
    protected PKiProvider getPKiProvider() {
        return mo_pKiProvider;
    }

    public byte[] getData() {
        return data.clone();
    }

    public void setData(byte[] data) {
        this.data = data.clone();
    }

    public byte[] getPkdata() {
        return pkdata.clone();
    }

    public void setPkdata(byte[] pkdata) {
        if (pkdata != null)
            this.pkdata = pkdata.clone();
        else
            this.pkdata = null;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPkpassword() {
        return pkpassword;
    }

    public void setPkpassword(String pkpassword) {
        this.pkpassword = pkpassword;
    }

    public void importP12() {
        StringBuffer dnsBuf = new StringBuffer();

        try {
            char[] p12Password = null;
            if (password == null) {
                p12Password = new char[0];
            } else {
                p12Password = password.toCharArray();
            }
            char[] keyPassword = null;
            if (pkpassword == null) {
                keyPassword = new char[0];
            } else {
                keyPassword = pkpassword.toCharArray();
            }

            ByteArrayInputStream fis = new ByteArrayInputStream(data);
            KeyStore keyStore = KeyStore.getInstance("PKCS12", getPKiProvider().getProviderName());
            keyStore.load(fis, p12Password);
            fis.close();

            Enumeration<String> aliases = keyStore.aliases();
            if (!aliases.hasMoreElements()) {
                throw new IllegalArgumentException("Invalid p12");
            }

            for (; aliases.hasMoreElements(); ) {
                String alias = aliases.nextElement();

                java.security.cert.Certificate[] certificates = keyStore.getCertificateChain(alias);
                X509Certificate x509Certificate = (X509Certificate) certificates[0];

                Key key = keyStore.getKey(alias, keyPassword);
                PrivateKey privateKey = (PrivateKey) key;

                Certificate certificate = CertificateManager.getCertificateFromX509(entityManager, x509Certificate,
                        new KeyPair(x509Certificate.getPublicKey(), privateKey));

                String subject = certificate.getSubject();
                Certificate existingCertificate = CertificateDAO.getUniqueBySubject(subject, entityManager);
                if (existingCertificate != null) {
                    throw new IllegalArgumentException(subject + " already exists in the database!");
                }

                entityManager.persist(certificate);

                dnsBuf.append(subject);
                dnsBuf.append(" ");
            }
        } catch (Exception e) {
            String msg = "Failed to import certificate (" + e.getMessage() + ")";
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg, e);
            log.error(msg, e);
            return;
        }
        FacesMessages.instance().add(StatusMessage.Severity.INFO, "Certificate imported ( " + dnsBuf.toString() + ")");
    }

    public void importPEM() {
        Certificate certificate = null;

        try {
            X509Certificate x509Certificate = null;
            KeyPair keys = null;
            if (data != null) {
                PKIXImporter importer = new PKIXImporter();

                x509Certificate = importer.getUniqueX509CertificateFromPEM(data);
                if (pkdata != null) {
                    keys = importer.getKeyPairFromPEM(pkdata, pkpassword);
                }
            } else {
                throw new IOException("No certificate file provided");
            }

            certificate = CertificateManager.getCertificateFromX509(entityManager, x509Certificate, keys);
            CertificateDAO.verifyNotAlreadyRegistered(certificate, entityManager);
            entityManager.persist(certificate);
            FacesMessages.instance().add(StatusMessage.Severity.INFO,
                    "Certificate imported (" + certificate.getSubject() + ")");
        } catch (Exception e) {
            String msg = "Failed to import certificate (" + e.getMessage() + ")";
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, msg, e);
            log.warn(msg, e);
        }
        setPkdata(null);
    }



    public void uploadListener(FileUploadEvent event) throws IOException {
        UploadedFile item = event.getUploadedFile();
        setData(item.getData());
    }

    public void uploadpkDataListener(FileUploadEvent event) throws IOException {
        UploadedFile item = event.getUploadedFile();
        setPkdata(item.getData());
    }


}