package net.ihe.gazelle.atna.tls.test.testinstance;

import net.ihe.gazelle.atna.action.ApplicationManagerBean;
import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.atna.tls.test.TlsTestDataSetDisplayer;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestInstance;
import net.ihe.gazelle.ssov7.gum.client.application.UserAttributeCommon;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Map;

@Name("testInstanceManager")
@Scope(ScopeType.PAGE)
public class TestInstanceManager extends AbstractTestInstanceBean implements Serializable, TlsTestDataSetDisplayer, UserAttributeCommon {

    private static final long serialVersionUID = 4994437896595399026L;

    private static Logger log = LoggerFactory.getLogger(TestInstanceManager.class);

    private int testInstanceId;
    private transient TlsTestInstance testInstance;

    @In(value="gumUserService")
    private UserService userService;

    public TlsTestInstance getTestInstance() {
        if(testInstance==null) {
            testInstance = EntityManagerService.provideEntityManager().find(TlsTestInstance.class, testInstanceId);
        }
        return testInstance;
    }

    @Create
    public void initialize() {

        Map<String, String> urlParameters = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap();

        if (urlParameters.get("id") != null) {
            try {
                testInstanceId = Integer.decode(urlParameters.get("id"));
                testInstance = EntityManagerService.provideEntityManager().find(TlsTestInstance.class,testInstanceId);
                if (testInstance == null) {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                            "No such test case recorded in database");
                    log.error("No such test case recorded in database");
                }
            } catch (NumberFormatException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "'" + urlParameters.get("testCase") + "' is not a valid test case id");
                log.error("'" + urlParameters.get("testCase") + "' is not a valid test case id", e);
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, "No test instance id provided");
            log.error("No test instance id provided");
        }
    }

    public String backToTest() {
        return Pages.TLS_VIEW_TEST_CASE.getMenuLink() + "?testCase=" + getTestInstance().getTestCaseId();
    }

    public String getTestCaseName() {
        return getTestInstance().getTestCase().getName();
    }

    @Override
    public String linkToCertificate() {
        if (testInstance.getTestDataSet() != null) {
            return ApplicationManagerBean.getUrl() + Pages.PKI_VIEW_CERTIFICATE.getMenuLink() +
                    "?id=" + testInstance.getTestDataSet().getCertificate().getId();
        } else {
            return null;
        }
    }

    @Override
    public String getUserName(String userId) {
        return userService.getUserDisplayNameWithoutException(userId);
    }
}
