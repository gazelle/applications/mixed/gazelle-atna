package net.ihe.gazelle.atna.tls.simulator;

import net.ihe.gazelle.atna.action.ApplicationManagerBean;
import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.atna.tls.server.ServerLauncher;
import net.ihe.gazelle.atna.tls.server.SimulatorForward;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.simulators.tls.model.TlsServer;
import net.ihe.gazelle.simulators.tls.model.TlsServerQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.annotations.security.Restrict;

import javax.persistence.EntityManager;
import java.util.Map;

/**
 * Created by cel on 17/09/15.
 */
@Name("serverSimulatorListBean")
@Scope(ScopeType.PAGE)
public class ServerSimulatorListBean extends AbstractSimulatorListBean<TlsServer> {

    private static final long serialVersionUID = -9146674829762411893L;
    private boolean showOnlyRunning = false;

    public boolean isShowOnlyRunning() {
        return showOnlyRunning;
    }

    public void setShowOnlyRunning(boolean showOnlyRunning) {
        this.showOnlyRunning = showOnlyRunning;
        this.getFilter().modified();
    }

    @Override
    public void resetFilter() {
        setShowOnlyEnabled(true);
        setShowOnlyRunning(false);
        this.getFilter().clear();
        this.getSimulators().resetCache();
    }

    @Override
    public String linkToNewSimulator() {
        return ApplicationManagerBean.getUrl() + Pages.TLS_EDIT_SERVER.getMenuLink();
    }

    @Override
    public String linkToViewSimulator(int simulatorId) {
        return ApplicationManagerBean.getUrl() + Pages.TLS_TEST_SERVER.getMenuLink() + "?simulator=" + simulatorId;
    }

    @Override
    public String linkToEditSimulator(int simulatorId) {
        return ApplicationManagerBean.getUrl() + Pages.TLS_EDIT_SERVER.getMenuLink() + "?id=" + simulatorId;
    }

    @Override
    protected HQLCriterionsForFilter<TlsServer> getHQLCriterionsForFilter() {
        TlsServerQuery query = new TlsServerQuery();
        HQLCriterionsForFilter<TlsServer> criterions = query.getHQLCriterionsForFilter();
        criterions.addPath("serverType", query.channelType());
        criterions.addQueryModifier(this);
        return criterions;
    }

    @Override
    public void modifyQuery(HQLQueryBuilder<TlsServer> hqlQueryBuilder, Map<String, Object> map) {
        super.modifyQuery(hqlQueryBuilder, map);
        if (isShowOnlyRunning()) {
            hqlQueryBuilder.addEq("running", true);
        }
    }

    @Override
    protected Class<TlsServer> getSimulatorClass() {
        return TlsServer.class;
    }

    @Restrict("#{s:hasRole('admin_role')}")
    public void startServer(int simulatorId) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        TlsServer simulator = entityManager.find(getSimulatorClass(), simulatorId);
        ServerLauncher.startServer(simulator);
        this.getFilter().modified();
    }

    @Restrict("#{s:hasRole('admin_role')}")
    public void stopServer(int simulatorId) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        TlsServer simulator = entityManager.find(getSimulatorClass(), simulatorId);
        ServerLauncher.stopServer(simulator);
        this.getFilter().modified();
    }

    @Override
    @Restrict("#{s:hasRole('admin_role')}")
    public void disableSimulator(int simulatorId) {
        EntityManager entityManager = EntityManagerService.provideEntityManager();
        TlsServer simulator = entityManager.find(getSimulatorClass(), simulatorId);
        ServerLauncher.stopServer(simulator);
        simulator.setEnabled(false);
        entityManager.merge(simulator);
        entityManager.flush();
        this.getFilter().modified();
    }

    public boolean isForwardReachable(TlsServer tlsServer) {
        if (tlsServer.isEnabled()) {
           return SimulatorForward.isReachable(tlsServer);
        } else {
            return false;
        }
    }

}
