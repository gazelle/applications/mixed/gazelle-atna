package net.ihe.gazelle.atna.tls.simulator;

import net.ihe.gazelle.atna.action.ApplicationManagerBean;
import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;
import net.ihe.gazelle.simulators.tls.model.TlsClient;
import net.ihe.gazelle.simulators.tls.model.TlsClientQuery;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

/**
 * Created by cel on 16/09/15.
 */
@Name("clientSimulatorListBean")
@Scope(ScopeType.PAGE)
public class ClientSimulatorListBean extends AbstractSimulatorListBean<TlsClient> {

    private static final long serialVersionUID = -1560937211447765806L;

    @Override
    protected HQLCriterionsForFilter<TlsClient> getHQLCriterionsForFilter() {
        TlsClientQuery query = new TlsClientQuery();
        HQLCriterionsForFilter<TlsClient> criterions = query.getHQLCriterionsForFilter();
        criterions.addQueryModifier(this);
        return criterions;
    }

    @Override
    protected Class<TlsClient> getSimulatorClass() {
        return TlsClient.class;
    }

    @Override
    public void resetFilter() {
        setShowOnlyEnabled(true);
        this.getFilter().clear();
        this.getSimulators().resetCache();
    }


    @Override
    public String linkToNewSimulator() {
        return ApplicationManagerBean.getUrl() + Pages.TLS_EDIT_CLIENT.getMenuLink();
    }

    @Override
    public String linkToViewSimulator(int simulatorId) {
        return ApplicationManagerBean.getUrl() + Pages.TLS_TEST_CLIENT.getMenuLink() + "?simulator=" + simulatorId;
    }

    @Override
    public String linkToEditSimulator(int simulatorId) {
        return ApplicationManagerBean.getUrl() + Pages.TLS_EDIT_CLIENT.getMenuLink() + "?id=" + simulatorId;
    }

}
