package net.ihe.gazelle.atna.pki;

import net.ihe.gazelle.atna.communication.Notification;
import net.ihe.gazelle.atna.questionnaire.preferences.UserAttributes;
import net.ihe.gazelle.pki.CertificateDAO;
import net.ihe.gazelle.pki.CertificateManager;
import net.ihe.gazelle.pki.CertificateUtil;
import net.ihe.gazelle.pki.enums.CertificateType;
import net.ihe.gazelle.pki.enums.KeyAlgorithm;
import net.ihe.gazelle.pki.enums.SignatureAlgorithm;
import net.ihe.gazelle.pki.model.Certificate;
import net.ihe.gazelle.pki.model.CertificateRequest;
import net.ihe.gazelle.pki.model.CertificateRequestWithGeneratedKeys;
import net.ihe.gazelle.preferences.PreferenceService;
import org.apache.commons.lang.StringUtils;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.io.Serializable;
import java.security.cert.CertificateException;
import java.util.Calendar;

/**
 * Extracted from ImportManager by Cedric EOCHE-DUVAL, KEREVAL on 11/07/16 (Refactoring)
 */
@Name("certificateCSVFactory")
@Scope(ScopeType.PAGE)
public class CertificateCSVFactory implements Serializable {

    private static final long serialVersionUID = 8273558245374076853L;
    private static final KeyAlgorithm KEY_ALGORITHM = KeyAlgorithm.RSA;
    private static final int KEY_LENGTH = 1024;

    private static final int CSV_COLUMN_NUMBER = 4;
    private static final int SYSTEM_CSV_INDEX = 0;
    private static final int ORGANISATION_CSV_INDEX = 1;
    private static final int OWNER_CSV_INDEX = 2;
    private static final int COUNTRY_CSV_INDEX = 3;

    private static Logger log = LoggerFactory.getLogger(CertificateCSVFactory.class);

    @In
    private transient EntityManager entityManager;
    private String csv;

    public CertificateCSVFactory() {
        super();
    }

    public String getCsv() {
        return csv;
    }

    public void setCsv(String csv) {
        this.csv = csv;
    }

    public void createCertificates() {

        Integer certificateId = PreferenceService.getInteger("certificate_authority_Id");

        if (certificateId != null) {
            Certificate certificateAuthority = CertificateDAO.getByID(certificateId, entityManager);
            String content = StringUtils.replace(csv, "\r\n", "\r");
            content = StringUtils.replace(content, "\n\r", "\r");
            content = StringUtils.replace(content, "\n", "\r");
            String[] lines = StringUtils.split(content, '\r');
            for (String line : lines) {
                if (StringUtils.trimToNull(line) != null) {
                    String[] elements = StringUtils.split(line, ',');
                    if (elements != null && elements.length == CSV_COLUMN_NUMBER) {
                        Certificate certificate = null;
                        try {
                            certificate = createOneCertificate(certificateAuthority, elements[SYSTEM_CSV_INDEX],
                                    elements[ORGANISATION_CSV_INDEX], elements[OWNER_CSV_INDEX],
                                    elements[COUNTRY_CSV_INDEX]);
                            FacesMessages.instance()
                                    .add(StatusMessage.Severity.INFO, "Created certificate for " + line);
                        } catch (CertificateException e) {
                            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                                    "Failed to create certificate for '" + line + "'");
                            log.warn("Failed to generate batch certificate (" + line + ")", e);
                        }
                        if (certificate != null) {
                            sendNotificationToTMUser(elements[2], certificate);
                        }
                    } else {
                        FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                                "Failed to create certificate for '" + line + "'");
                        log.warn("Failed to generate batch certificate for line '" + line + "'");
                    }
                }
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                    "Batch generation disabled. Define a Certificate Authority to the system first.");
        }
    }

    private Certificate createOneCertificate(Certificate certificateAuthority, String system,
                                             String organisation, String owner, String country)
            throws CertificateException {

        CertificateRequest certificateRequest = new CertificateRequestWithGeneratedKeys(KEY_ALGORITHM, KEY_LENGTH);

        certificateRequest.setCertificateAuthority(certificateAuthority);
        certificateRequest.setCertificateExtension(CertificateType.CLIENT_AND_SERVER);
        certificateRequest.setRequester(owner, UserAttributes.instance().getCasKeyword());
        /*FIXME Currently the CAS keyword is the admin's one. However Admin should be able to select for which CAS
        keyword certificates are generated.*/

        String year = Integer.toString(Calendar.getInstance().get(Calendar.YEAR));
        certificateRequest.setSubjectUsingAttributes(country, organisation, system, null, null, null, "CAT-" + year,
                null);
        entityManager.persist(certificateRequest);

        certificateRequest.setNotAfter(CertificateUtil.getNotAfter());
        certificateRequest.setNotBefore(CertificateUtil.getNotBefore());
        certificateRequest.setSignatureAlgorithm(SignatureAlgorithm.SHA512WITHRSAENCRYPTION);
        Certificate certificate = CertificateManager.createCertificate(certificateRequest, entityManager);
        entityManager.flush();
        return certificate;
    }

    /**
     * It sends a notification about the certificate creation to the owner in Gazelle TM.
     *
     * @param owner
     * @param certificate
     */
    private void sendNotificationToTMUser(String owner, Certificate certificate) {
        String message = "Your certificate has been signed by an admin.";
        Notification.sendMessage(message, owner, PreferenceService.getString("application_url")
                + "/details/cert.seam?id=" + certificate.getId());
    }

}
