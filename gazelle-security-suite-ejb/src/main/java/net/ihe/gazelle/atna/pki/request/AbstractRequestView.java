package net.ihe.gazelle.atna.pki.request;

import net.ihe.gazelle.pki.model.CertificateRequest;
import net.ihe.gazelle.pki.model.CertificateRequestCSR;

/**
 * Created by cel on 24/02/16.
 */
public class AbstractRequestView {

    private static final String CAS_AUTO_LOGIN_REQUEST_DISTINGUISHER = "T=CAS,GIVENNAME=";

    public boolean isCasAutoLoginRequest(CertificateRequest request) {
        if (request != null && request.getSubject() != null &&
                request.getSubject().contains(CAS_AUTO_LOGIN_REQUEST_DISTINGUISHER)) {
            return true;
        }
        return false;
    }

    public boolean isUploadedCSRRequest(CertificateRequest request) {
        if (request != null && request.getClass().equals(CertificateRequestCSR.class) &&
                !isCasAutoLoginRequest(request)) {
            return true;
        }
        return false;
    }

}
