package net.ihe.gazelle.atna.pki.request;

import net.ihe.gazelle.common.jsf.SelectItemConverter;
import net.ihe.gazelle.pki.CertificateUtil;
import net.ihe.gazelle.pki.enums.KeySize;
import org.jboss.seam.annotations.In;
import org.jboss.seam.international.LocaleSelector;

import javax.faces.model.SelectItem;
import java.security.cert.CertificateException;

/**
 * Created by Cedric EOCHE-DUVAL, KEREVAL on 12/07/16.
 */
public class RequestWithSubjectCreator extends AbstractRequestCreator {


    private RequestSubject requestSubject;
    private KeySize keySize;
    @In
    private LocaleSelector localeSelector;

    public void init() throws CertificateException {
        requestSubject = new RequestSubject() ;
        keySize = DEFAULT_KEY_SIZE;
    }

    public String getCountry() {
        return requestSubject.getCountry();
    }

    public void setCountry(String country) {
        requestSubject.setCountry(country);
    }

    public String getSubjectAltName() {
        return requestSubject.getSubjectAltName();
    }

    public void setSubjectAltName(String subjectAltName) {
        requestSubject.setSubjectAltName(subjectAltName);
    }

    public String getOrganization() {
        return requestSubject.getOrganization();
    }

    public void setOrganization(String organization) {
        requestSubject.setOrganization(organization);
    }

    public String getCommonName() {
        return requestSubject.getCommonName();
    }

    public void setCommonName(String commonName) {
        requestSubject.setCommonName(commonName);
    }

    public String getTitle() {
        return requestSubject.getTitle();
    }

    public void setTitle(String title) {
        requestSubject.setTitle(title);
    }

    public String getGivenName() {
        return requestSubject.getGivenName();
    }

    public void setGivenName(String givenName) {
        requestSubject.setGivenName(givenName);
    }

    public String getSurname() {
        return requestSubject.getSurname();
    }

    public void setSurname(String surname) {
        requestSubject.setSurname(surname);
    }

    public String getOrganizationalUnit() {
        return requestSubject.getOrganizationalUnit();
    }

    public void setOrganizationalUnit(String organizationalUnit) {
        requestSubject.setOrganizationalUnit(organizationalUnit);
    }

    public String geteMail() {
        return requestSubject.geteMail();
    }

    public void seteMail(String eMail) {
        requestSubject.seteMail(eMail);
    }

    public KeySize getKeySize() {
        return keySize;
    }

    public void setKeySize(KeySize keySize) {
        this.keySize = keySize;
    }

    public SelectItem[] getKeySizes() {
        return SelectItemConverter.asSelectItems(KeySize.getIHEAllowedKeySizes());
    }

    public SelectItem[] getCountries() {
        return CertificateUtil.getCountries(localeSelector.getLocale());
    }

}
