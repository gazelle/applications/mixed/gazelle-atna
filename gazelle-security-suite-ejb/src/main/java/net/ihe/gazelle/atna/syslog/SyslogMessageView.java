package net.ihe.gazelle.atna.syslog;

import net.ihe.gazelle.atna.menu.Pages;
import net.ihe.gazelle.common.util.XmlFormatter;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.syslog.model.SyslogCollectorMessage;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.Create;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.jboss.seam.faces.FacesMessages;
import org.jboss.seam.international.StatusMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.faces.context.FacesContext;
import java.io.Serializable;
import java.util.Map;

/**
 * Created by cel on 07/09/16.
 */
@Name("syslogMessageView")
@Scope(ScopeType.PAGE)
public class SyslogMessageView extends SyslogCommonBean implements Serializable {

    private static final long serialVersionUID = 4158528236115957105L;
    private static Logger log = LoggerFactory.getLogger(SyslogMessageView.class);
    private static String syslogMessageListbaseLink = PreferenceService.getString("application_url") + Pages.SYSLOG_LIST
            .getMenuLink();
    private static String syslogMessageBaseLink = PreferenceService.getString("application_url") + Pages.SYSLOG_VIEW
            .getMenuLink();


    private static final String NO_SUCH_RECORD = "No such syslog message recorded in database";
    private static final String INVALID_ID = "' is not a valid syslog message id";
    private static final String MISSING_PARAM = "Missing parameter 'id', unable to retrieve syslog message.";

    private int messageId = 0;
    private SyslogCollectorMessage syslogMessage;
    private boolean ready = false;

    @Create
    public void init() throws IllegalArgumentException {
        Map<String, String> urlParameters = FacesContext.getCurrentInstance().getExternalContext()
                .getRequestParameterMap();

        if (urlParameters.get("id") != null) {
            try {
                messageId = Integer.decode(urlParameters.get("id"));
                syslogMessage = EntityManagerService.provideEntityManager().find(SyslogCollectorMessage.class,
                        messageId);
                if (syslogMessage != null) {
                    ready = true;
                } else {
                    FacesMessages.instance().add(StatusMessage.Severity.ERROR, NO_SUCH_RECORD);
                    log.warn(NO_SUCH_RECORD);
                }
            } catch (NumberFormatException e) {
                FacesMessages.instance().add(StatusMessage.Severity.ERROR,
                        "'" + urlParameters.get("id") + INVALID_ID);
                log.warn("'" + urlParameters.get("id") + INVALID_ID, e);
            }
        } else {
            FacesMessages.instance().add(StatusMessage.Severity.ERROR, MISSING_PARAM);
            log.warn(MISSING_PARAM);
        }
    }

    public boolean isReady() {
        return ready;
    }

    public SyslogCollectorMessage getSyslogMessage() {
        if (syslogMessage == null && messageId != 0) {
            //if bean is serialized
            syslogMessage = EntityManagerService.provideEntityManager().find(SyslogCollectorMessage.class, messageId);
        }
        return syslogMessage;
    }

    public String getXMLFormated(String message) {
        if (message != null && !message.isEmpty()) {
            try {
                return XmlFormatter.format(message);
            } catch (Exception e) {
                // It may failed to format message to XML if it is plain text.
                return message;
            }
        } else {
            return null;
        }
    }

    public String getSyslogMessageListLink() {
        return syslogMessageListbaseLink;
    }

    public String getPermanentLink(SyslogCollectorMessage syslogMessage) {
        if (syslogMessage != null) {
            return syslogMessageBaseLink + "?id=" + syslogMessage.getId();
        } else {
            return getSyslogMessageListLink();
        }
    }

}
