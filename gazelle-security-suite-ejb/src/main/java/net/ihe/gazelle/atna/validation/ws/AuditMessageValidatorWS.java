package net.ihe.gazelle.atna.validation.ws;

import java.io.Serializable;

import javax.ejb.Stateless;
import javax.jws.WebService;

import net.ihe.gazelle.audit.message.validator.GlobalAuditMessageValidator;
import net.ihe.gazelle.audit.message.ws.AbstractAMValidatorWS;

import net.ihe.gazelle.validation.exception.GazelleValidationException;
import net.ihe.gazelle.validation.model.ValidatorDescription;
import org.jboss.seam.annotations.Name;

@Stateless
@Name("auditMessageValidatorWS")
@WebService(name = "AuditMessageValidationWS", serviceName = "AuditMessageValidationWSService", portName = "AuditMessageValidationWSPort", targetNamespace = "http://ws.mb.validator.gazelle.ihe.net")
public class AuditMessageValidatorWS extends AbstractAMValidatorWS implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    static {
        GlobalAuditMessageValidator.globalPreferenceProvider = new GlobalPreferenceProviderImpl();
    }

    @Override
    protected String buildReportOnParsingFailure(GazelleValidationException e, ValidatorDescription selectedValidator) {
        return null;
    }

    @Override
    protected String executeValidation(String document, ValidatorDescription validator, boolean extracted) throws GazelleValidationException {
        return null;
    }
}