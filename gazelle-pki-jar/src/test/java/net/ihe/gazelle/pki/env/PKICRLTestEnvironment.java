package net.ihe.gazelle.pki.env;

import net.ihe.gazelle.preferences.PreferenceService;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by cel on 22/07/15.
 */
@PowerMockIgnore({"javax.*", "net.ihe.gazelle.hql.providers.*"})
@PrepareForTest({PreferenceService.class})
@RunWith(PowerMockRunner.class)
public abstract class PKICRLTestEnvironment extends PKITestEnvironment {

    private static final Logger LOG = LoggerFactory.getLogger(PKICRLTestEnvironment.class);

    private static CrlServer crlServer;
    private static int crlServerUsage = 0;

    /*******
     * SETUP / TEARDOWN
     ******/

    @Before
    public void setUp() throws Exception {
        LOG.info("Setup CRL Server");
        setUpCrlServer();
        super.setUp(); //JPA + Transaction + Bouncycastle setup
    }

    @After
    public void tearDown() throws Exception {
        super.tearDown(); //JPA + Transaction + Bouncycastle teardown
        LOG.info("Teardown CRL Server");
        tearDownCrlServer();
    }

    /******
     * CRL SERVER
     *****/

    protected synchronized static void setUpCrlServer() {
        if (crlServer == null) {
            crlServer = new CrlServer(TestParam.CRL_PORT, false);
            crlServer.start();

        }
        crlServerUsage++;
        LOG.info("crlServer +1 usage = " + crlServerUsage);
    }


    protected synchronized static void tearDownCrlServer() {
        if (crlServerUsage == 1) {
            crlServer.stop();
            crlServer = null;
        }
        crlServerUsage--;
        LOG.info("crlServer -1 usage = " + crlServerUsage);
    }

}
