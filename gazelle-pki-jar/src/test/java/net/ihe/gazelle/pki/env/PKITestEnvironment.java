package net.ihe.gazelle.pki.env;

import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.junit.AbstractTestQueryJunit4;
import net.ihe.gazelle.pki.bouncycastle.CertificateBC;
import net.ihe.gazelle.pki.model.PKiProvider;
import net.ihe.gazelle.preferences.PreferenceService;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.awt.*;
import java.security.Security;

/**
 * Created by cel on 22/07/15.
 */
@PowerMockIgnore({"javax.*", "net.ihe.gazelle.hql.providers.*"})
@PrepareForTest({PreferenceService.class, org.jboss.seam.Component.class})
@RunWith(PowerMockRunner.class)
public abstract class PKITestEnvironment extends AbstractTestQueryJunit4 {

    private static final Logger LOG = LoggerFactory.getLogger(PKITestEnvironment.class);

    private static String BCproviderName;
    private static EntityManager mo_entityManagerInstance;

    protected void mockPreferences() {
        PowerMockito.mockStatic(PreferenceService.class);
        Mockito.when(PreferenceService.getString("crl_url")).thenReturn(TestParam.CRL_URL);
        Mockito.when(PreferenceService.getString("java_cacerts_truststore_pwd")).thenReturn(TestParam.CACERTS_PASSWORD);

      /*  PowerMockito.mockStatic(org.jboss.seam.Component.class);
        Mockito.when(org.jboss.seam.Component.getInstance("CertificateBC", true)).thenReturn(new CertificateBC()); */
    }

    /*******
     * SETUP / TEARDOWN
     ******/

    @Before
    public void setUp() throws Exception {
        mockPreferences();
        setUpBouncyCastle();
        super.setUp(); //JPA setup
        LOG.info("Begin transaction");
        if (mo_entityManagerInstance == null)
            mo_entityManagerInstance = EntityManagerService.provideEntityManager();
        mo_entityManagerInstance.getTransaction().begin();
    }

    @After
    public void tearDown() throws Exception {
        LOG.info("Commit transaction");
        mo_entityManagerInstance.getTransaction().commit();
        super.tearDown(); //JPA teardown
        tearDownBouncyCastle();
    }

    /******
     * GAZELLE JUNIT SETUP
     ******/

    protected boolean getShowSql() {
        return true;
    }

    /******
     * BOUNCYCASTLE
     ******/

    protected static void setUpBouncyCastle() {
        //PKiProvider pKiProvider =  (PKiProvider) org.jboss.seam.Component.getInstance("CertificateBC", true);
        PKiProvider pKiProvider = new CertificateBC();

        LOG.info("Add bouncycastle provider");
        BCproviderName = pKiProvider.getProviderName();
        Security.addProvider(pKiProvider.createProvider());
    }

    protected static void tearDownBouncyCastle() {
        LOG.info("Remove bouncycastle provider");
        Security.removeProvider(BCproviderName);
    }

}
