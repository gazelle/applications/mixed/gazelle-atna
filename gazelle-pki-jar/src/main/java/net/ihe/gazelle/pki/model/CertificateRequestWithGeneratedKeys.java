package net.ihe.gazelle.pki.model;

import net.ihe.gazelle.pki.enums.KeyAlgorithm;

import javax.persistence.Entity;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.cert.CertificateException;

/**
 * The aim of this class is to generate a certificate request and its associated key pair. Once the request will be accepted, the signed certificate AND the key pair will be delivered to the requester.
 */
@Entity
public class CertificateRequestWithGeneratedKeys extends CertificateRequest {

    /**
     * Instantiates a new certificate request auto sign. Only for Hibernate.
     */
    protected CertificateRequestWithGeneratedKeys() {
        super();
    }

    public CertificateRequestWithGeneratedKeys(KeyAlgorithm keyAlgorithm, int keySize) throws CertificateException {
        super();
        KeyPair keyPair;
        try {
            KeyPairGenerator keygen = KeyPairGenerator.getInstance(keyAlgorithm.getKeySpec(),
                    getPKiProvider().getProviderName());
            keygen.initialize(keySize);
            keyPair = keygen.generateKeyPair();

            setPublicKey(keyPair.getPublic());
            setPrivateKey(keyPair.getPrivate());
        } catch (NoSuchProviderException e) {
            throw new CertificateException(e);
        } catch (NoSuchAlgorithmException e) {
            throw new CertificateException(e);
        }
    }

}
