package net.ihe.gazelle.pki.model;

import java.security.PrivateKey;
import java.security.cert.CertificateException;

import javax.persistence.Entity;

@Entity
public class CertificatePrivateKey extends CertificateKey<PrivateKey> {

    public CertificatePrivateKey() {
        super();
    }

    public CertificatePrivateKey(PrivateKey key) throws CertificateException {
        super(key);
    }

}
