package net.ihe.gazelle.pki.crl;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.persistence.EntityManager;
import javax.servlet.GenericServlet;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import net.ihe.gazelle.hql.providers.detached.HibernateActionPerformer;
import net.ihe.gazelle.hql.providers.detached.HibernateFailure;
import net.ihe.gazelle.hql.providers.detached.PerformHibernateAction;

public class CrlServlet extends GenericServlet {

    private static final long serialVersionUID = 5468080500868270913L;

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        int id = -1;
        if (req instanceof HttpServletRequest) {
            HttpServletRequest httpReq = (HttpServletRequest) req;
            String uri = httpReq.getRequestURL().toString();

            // Decode the path.
            try {
                uri = URLDecoder.decode(uri, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                try {
                    uri = URLDecoder.decode(uri, "ISO-8859-1");
                } catch (UnsupportedEncodingException e1) {
                    throw new ServletException();
                }
            }

            int lastIndexOf = uri.lastIndexOf("/cacrl.crl");
            if (lastIndexOf != -1) {
                uri = uri.substring(0, lastIndexOf);
                lastIndexOf = uri.lastIndexOf("/");
                if (lastIndexOf != -1) {
                    uri = uri.substring(lastIndexOf + 1);
                    try {
                        id = Integer.parseInt(uri);
                    } catch (Exception e) {
                        throw new ServletException(e);
                    }
                }
            }

            if (id >= 0) {
                final int crlId = id;
                final ServletResponse servletResponse = res;
                byte[] crl;
                try {
                    crl = (byte[]) HibernateActionPerformer.performHibernateAction(new PerformHibernateAction() {
                        @Override
                        public Object performAction(EntityManager entityManager, Object... context) throws Exception {
                            byte[] crl = null;
                            crl = CrlUtil.generateCRL(crlId, entityManager);
                            return crl;
                        }
                    });
                } catch (HibernateFailure e) {
                    throw new ServletException("Failed to get informations from database");
                }
                if (crl == null) {
                    throw new ServletException("Certificate not found");
                }
                servletResponse.getOutputStream().write(crl, 0, crl.length);
            }
        }
    }
}
