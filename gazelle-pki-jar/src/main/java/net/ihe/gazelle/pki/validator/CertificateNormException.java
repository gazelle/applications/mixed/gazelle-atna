package net.ihe.gazelle.pki.validator;

import java.security.cert.CertificateException;

public class CertificateNormException extends CertificateException {

    private static final long serialVersionUID = 9153461555903250516L;

    private String norm;

    public CertificateNormException() {
        super();
    }

    public CertificateNormException(String message, String norm, Throwable cause) {
        super(message, cause);
        this.norm = norm;
    }

    public CertificateNormException(String msg, String norm) {
        super(msg);
        this.norm = norm;
    }

    public CertificateNormException(Throwable cause, String norm) {
        super(cause);
        this.norm = norm;
    }

    public String getNorm() {
        return norm;
    }

    public void setNorm(String norm) {
        this.norm = norm;
    }

}
