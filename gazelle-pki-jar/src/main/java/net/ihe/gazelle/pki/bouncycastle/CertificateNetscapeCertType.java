package net.ihe.gazelle.pki.bouncycastle;

import org.bouncycastle.asn1.misc.NetscapeCertType;
import org.bouncycastle.jce.netscape.NetscapeCertRequest;

import java.io.IOException;
import java.security.PublicKey;

/**
 * The NetscapeCertType object.
 * <pre>
 *    NetscapeCertType ::= BIT STRING {
 *         SSLClient               (0),
 *         SSLServer               (1),
 *         S/MIME                  (2),
 *         Object Signing          (3),
 *         Reserved                (4),
 *         SSL CA                  (5),
 *         S/MIME CA               (6),
 *         Object Signing CA       (7) }
 * </pre>
 */
public class CertificateNetscapeCertType extends NetscapeCertType {
    public CertificateNetscapeCertType(int usage) {
        super(usage);
    }

    /**
     *
     * @param po_bts
     * @return
     * @throws IOException
     */
    public static PublicKey getNetscapeCertRequest(byte[] po_bts) throws IOException {
        NetscapeCertRequest netscapeCertRequest = new NetscapeCertRequest(po_bts);
        return netscapeCertRequest.getPublicKey();
    }


}
