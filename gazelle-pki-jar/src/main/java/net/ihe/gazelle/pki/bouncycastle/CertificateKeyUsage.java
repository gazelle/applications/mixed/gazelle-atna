package net.ihe.gazelle.pki.bouncycastle;

import net.ihe.gazelle.pki.validator.CertificateNormException;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.x509.KeyUsage;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.util.List;

import static net.ihe.gazelle.pki.validator.epsos.v4.EpsosValidator.EHEALTH_X509_3_1;

/**
 * <pre>
 * KeyUsage ::= BIT STRING {
 *            digitalSignature        (0),
 *            nonRepudiation          (1), -- recent editions of X.509 have
 *                                 -- renamed this bit to contentCommitment
 *            keyEncipherment         (2),
 *            dataEncipherment        (3),
 *            keyAgreement            (4),
 *            keyCertSign             (5),
 *            cRLSign                 (6),
 *            encipherOnly            (7),
 *            decipherOnly            (8) }
 * </pre>
 */
public class CertificateKeyUsage {
    public static final int DIGITAL_SIGNATURE = KeyUsage.digitalSignature;
    public static final int NON_REPUDIATION = KeyUsage.nonRepudiation;
    public static final int KEY_ENCIPHERMENT = KeyUsage.keyEncipherment;
    public static final int DATA_ENCIPHERMENT = KeyUsage.dataEncipherment;
    public static final int KEY_AGREEMENT = KeyUsage.keyAgreement;
    public static final int KEY_CERT_SIGN = KeyUsage.keyCertSign;
    public static final int CRL_SIGN = KeyUsage.cRLSign;
    public static final int ENCIPHER_ONLY = KeyUsage.encipherOnly;
    public static final int DECIPHER_ONLY = KeyUsage.decipherOnly;

    private static CertificateKeyUsage mo_instance;

    /**
     *
     */
    private CertificateKeyUsage() { }

    /**
     *
     * @return
     */
    public static CertificateKeyUsage getInstance() {
        if (mo_instance == null)
            mo_instance = new CertificateKeyUsage();

        return mo_instance;
    }

    /**
     *
     * @param po_keyUsageBytes
     * @param po_exceptions
     * @return
     */
    public int getkeyUsageBytes(byte[] po_keyUsageBytes, List<CertificateException> po_exceptions) {
        ASN1Primitive obj;
        try {
            obj = org.bouncycastle.cert.jcajce.JcaX509ExtensionUtils.parseExtensionValue(po_keyUsageBytes);
        } catch (IOException e) {
            po_exceptions.add(new CertificateNormException(e, EHEALTH_X509_3_1));
            return -1;
        }
        KeyUsage keyUsage = KeyUsage.getInstance(obj);
        return keyUsage.getBytes()[0] & 0xff;
    }

    /**
     *
     * @return
     */
    public static int getKeyUsageEncipherment() { return KeyUsage.keyEncipherment; }

    /**
     *
     * @return
     */
    public static int getKeyUsageDigitalSignature() {
        return KeyUsage.digitalSignature;
    }

    /**
     *
     * @return
     */
    public static int getKeyUsageNonRepudiation() {
        return KeyUsage.nonRepudiation;
    }

    /**
     *
     * @return
     */
    public static int getKeyUsageKeyEncipherment() {
        return KeyUsage.keyEncipherment;
    }

    /**
     *
     * @return
     */
    public static int getKeyUsageDataEncipherment() {
        return KeyUsage.dataEncipherment;
    }

    /**
     *
     * @return
     */
    public static int getKeyUsageKeyAgreement() {
        return KeyUsage.keyAgreement;
    }

    /**
     *
     * @return
     */
    public static int getKeyUsageKeyCertSign() {
        return KeyUsage.keyCertSign;
    }

    /**
     *
     * @return
     */
    public static int getKeyUsageCRLSign() {
        return KeyUsage.cRLSign;
    }

    /**
     *
     * @return
     */
    public static int getKeyUsageEncipherOnly() {
        return KeyUsage.encipherOnly;
    }

    /**
     *
     * @return
     */
    public static int getKeyUsageDecipherOnly() {
        return KeyUsage.decipherOnly;
    }

    /**
     *
     * @return
     */
    public static int[] getTLSKeyUsageBits() {
        int[] lo_return = {KeyUsage.digitalSignature | KeyUsage.keyEncipherment, KeyUsage.digitalSignature | KeyUsage.keyEncipherment | KeyUsage
                .dataEncipherment};
        return lo_return;
    }

    /**
     *
     * @return
     */
    public static int getSealKeyUsageBits() {
        return KeyUsage.digitalSignature | KeyUsage.keyEncipherment;
    }

}
