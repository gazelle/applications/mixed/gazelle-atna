package net.ihe.gazelle.pki.bouncycastle;

import org.bouncycastle.asn1.ASN1ObjectIdentifier;
import org.bouncycastle.asn1.x500.style.BCStrictStyle;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x500.style.RFC4519Style;
import org.bouncycastle.asn1.x509.KeyPurposeId;

import java.util.Hashtable;

class CertificateConstants {
    static final String EP_SOS_WP_3_4_2_CHAPTER_5_4_1 = "epSOS WP 3.4.2 - Chapter 5.4.1";
    static final String EP_SOS_WP_3_4_2_CHAPTER_5_4_2 = "epSOS WP 3.4.2 - Chapter 5.4.2";

    static final KeyPurposeId[] KEY_PURPOSE_IDS = new KeyPurposeId[]{KeyPurposeId.anyExtendedKeyUsage,
            KeyPurposeId.id_kp_capwapAC, KeyPurposeId.id_kp_clientAuth, KeyPurposeId.id_kp_capwapWTP,
            KeyPurposeId.id_kp_codeSigning, KeyPurposeId.id_kp_dvcs, KeyPurposeId.id_kp_eapOverLAN,
            KeyPurposeId.id_kp_eapOverPPP, KeyPurposeId.id_kp_emailProtection, KeyPurposeId.id_kp_ipsecEndSystem,
            KeyPurposeId.id_kp_ipsecIKE, KeyPurposeId.id_kp_ipsecTunnel, KeyPurposeId.id_kp_ipsecUser,
            KeyPurposeId.id_kp_OCSPSigning, KeyPurposeId.id_kp_sbgpCertAAServerAuth, KeyPurposeId.id_kp_scvp_responder,
            KeyPurposeId.id_kp_scvpClient, KeyPurposeId.id_kp_scvpServer, KeyPurposeId.id_kp_serverAuth,
            KeyPurposeId.id_kp_smartcardlogon, KeyPurposeId.id_kp_timeStamping};


    static ASN1ObjectIdentifier RFC4519StyleC = RFC4519Style.c;
    static ASN1ObjectIdentifier BCStyleC = BCStyle.C;
    static ASN1ObjectIdentifier BCStyleO = BCStyle.O;
    static ASN1ObjectIdentifier BCStyleCN = BCStyle.CN;
    static ASN1ObjectIdentifier BCStyleOU = BCStyle.OU;
    static ASN1ObjectIdentifier BCStyleDN_QUALIFIER = BCStyle.DN_QUALIFIER;

    public static String oidToDisplayName(ASN1ObjectIdentifier po_objectIdentifier){
        BCStyle bcStyle = new BCStrictStyle();
        return bcStyle.oidToDisplayName(po_objectIdentifier);
    }
}
