package net.ihe.gazelle.pki.bouncycastle;

import net.ihe.gazelle.pki.validator.CertificateNormException;
import org.bouncycastle.asn1.*;
import org.bouncycastle.asn1.x509.*;
import org.bouncycastle.asn1.x509.qualified.QCStatement;

import java.io.IOException;
import java.security.cert.CertificateException;
import java.security.cert.X509CRL;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;


import static net.ihe.gazelle.pki.validator.epsos.v4.EpsosValidator.EHEALTH_X509_3_1;

public class CertificateExtentionChecker {
    private static CertificateExtentionChecker mo_instance;

    /**
     *
     */
    private CertificateExtentionChecker() { }

    /**
     *
     * @return
     */
    public static CertificateExtentionChecker getInstance() {
        if (mo_instance == null)
            mo_instance = new CertificateExtentionChecker();

        return mo_instance;
    }

    /**
     *
     * @param ps_oid
     * @param po_ext
     * @return
     * @throws CertificateException
     */
    public static ASN1Primitive getObject(String ps_oid, byte[] po_ext) throws CertificateException {
        ASN1Primitive asn1Primitive = null;

        try (ASN1InputStream aIn1 = new ASN1InputStream(po_ext)) {
            ASN1OctetString octs = (ASN1OctetString) aIn1.readObject();
            try (ASN1InputStream aIn2 = new ASN1InputStream(octs.getOctets())) {
                asn1Primitive = aIn2.readObject();
            } catch (IOException ioe) {
                throw new CertificateException("exception processing extension " + ps_oid, ioe);
            }
        } catch (IOException ioe) {
            throw new CertificateException("exception processing extension " + ps_oid, ioe);
        }
        return asn1Primitive;
    }

    /**
     * Extract the value of the given extension, if it exists.
     *
     * @param ext The extension object.
     * @param oid The object identifier to obtain.
     * @return the extension as {@link ASN1Primitive}
     * @throws CertificateException if the extension cannot be read.
     */
    protected static ASN1Primitive getExtensionValue(java.security.cert.X509Extension ext, String oid)
            throws CertificateException {
        byte[] bytes = ext.getExtensionValue(oid);
        if (bytes == null) {
            return null;
        }

        return getObject(oid, bytes);
    }

    /**
     *
     * @param po_crl
     * @param po_warnings
     */
    public void checkX509CRLExtention(X509CRL po_crl, List<CertificateException> po_warnings) {
        try {
            ASN1Primitive issuerAltNameObject = getExtensionValue(po_crl, CertificateExtention.getIssuerAlternativeNameObjectId());
            if (issuerAltNameObject != null) {
                //TODO
            } else {
                po_warnings.add(new CertificateNormException("\"IssuerAltNames\" MAY be an extension in the CRL.",
                        EHEALTH_X509_3_1));
            }
        } catch (CertificateException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param x509Certificate
     * @return
     */
    private List<AccessDescription[]> getAccessDescriptions(X509Certificate x509Certificate) {
        List<AccessDescription[]> accessDescriptionsList = new ArrayList<>();
        AuthorityInformationAccess authorityInformationAccess = null;
        try {
            ASN1Primitive authorityInfoAccessObject = getExtensionValue(x509Certificate, CertificateExtention.getAuthorityInfoAccessObjectId());
            if (authorityInfoAccessObject != null) {
                if (authorityInfoAccessObject instanceof ASN1Sequence) {
                    ASN1Sequence seq = (ASN1Sequence) authorityInfoAccessObject;
//                    if (seq.size() == 2) {
//                        AuthorityInformationAccess authorityInformationAccess = AuthorityInformationAccess.getInstance(seq);
//                        accessDescriptionsList.add(authorityInformationAccess.getAccessDescriptions());
//                    }
                    authorityInformationAccess = AuthorityInformationAccess.getInstance(seq);
                }
            }
        } catch (CertificateException|IllegalArgumentException e) {
            e.printStackTrace();
        }
        if (authorityInformationAccess != null){
            accessDescriptionsList.add(authorityInformationAccess.getAccessDescriptions());
        }
        return accessDescriptionsList;
    }

    /**
     *
     * @param po_x509Certificate
     * @return
     * @throws CertificateException
     */
    public String getPolicyIdentifier(X509Certificate po_x509Certificate) throws CertificateException {
        String ls_return = "";
        ASN1Primitive certificatePoliciesObject = getExtensionValue(po_x509Certificate, CertificateExtention.getCertificatePoliciesObjectId());
        if (certificatePoliciesObject != null) {
            if (certificatePoliciesObject instanceof ASN1Primitive) {
                ASN1Sequence seq = (ASN1Sequence) certificatePoliciesObject;

                for (int pos = 0; pos < seq.size(); pos++) {
                    PolicyInformation policyInformation = PolicyInformation.getInstance(seq.getObjectAt(pos));
                    ls_return = policyInformation.getPolicyIdentifier().getId();
                }
            }
        }
        return ls_return;
    }

    /**
     *
     * @param qcStatement
     * @param exceptions
     */
    protected void validateQCStatementUsages(QCStatement qcStatement, List<CertificateException> exceptions) {
        if (qcStatement.getStatementId() != QCStatement.id_etsi_qct_web) {
            exceptions.add(new CertificateNormException(
                    "The esi4-qcStatement-6 id-etsi-qcs-QcType 3 statement SHOULD be included on its own as specified in [ETSI EN 319 412-5] to " +
                            "indicate that it is used for the purposes of electronic website authentication.", EHEALTH_X509_3_1));
        }
        if (qcStatement.getStatementId() != QCStatement.id_etsi_qct_eseal) {
            exceptions.add(new CertificateNormException(
                    "The esi4-qcStatement-6 id-etsi-qcs-QcType 2 SHOULD be included on its own as specified in [ETSI EN 319 412-5] to indicate that" +
                            " it is used for the purposes of electronic seal.", EHEALTH_X509_3_1));
        }
    }

    /**
     *
     * @param po_x509Certificate
     * @param po_exceptions
     */
    public void validateQCStatementUsages(X509Certificate po_x509Certificate, List<CertificateException> po_exceptions) {
        try {
            ASN1Primitive qcStatementObject = getExtensionValue(po_x509Certificate, CertificateExtention.getQCStatementsObjectId());
            if (qcStatementObject != null) {
                if (qcStatementObject instanceof ASN1Sequence) {
                    ASN1Sequence seq = (ASN1Sequence) qcStatementObject;
                    QCStatement qcStatement = QCStatement.getInstance(seq);
                    validateQCStatementUsages(qcStatement, po_exceptions);
                }
            }
        } catch (CertificateException e) {
            e.printStackTrace();
        }
    }

    /**
     *
     * @param cert
     * @return
     * @throws CertificateException
     */
    private String getOCSPFromAuthorityInfoAccessV1(X509Certificate cert) throws CertificateException {
        ASN1Primitive authInfoAcc = getExtensionValue(cert, CertificateExtention.getAuthorityInfoAccessObjectId());
        if (authInfoAcc != null) {
            if (authInfoAcc instanceof ASN1Sequence) {
                ASN1Sequence seq = (ASN1Sequence) authInfoAcc;
                Enumeration e = seq.getObjects();
                while (e.hasMoreElements()) {
                    Object nextElement = e.nextElement();
                    try {
                        AuthorityInformationAccess authInfoAccess = AuthorityInformationAccess.getInstance(nextElement);
                        if (authInfoAccess != null) {
                            AccessDescription[] ads = authInfoAccess.getAccessDescriptions();
                            for (int i = 0; i < ads.length; i++) {
                                if (ads[i].getAccessMethod().equals(AccessDescription.id_ad_ocsp)) {
                                    GeneralName name = ads[i].getAccessLocation();
                                    if (name.getTagNo() == GeneralName.uniformResourceIdentifier) {
                                        return ((DERIA5String) name.getName()).getString();
                                    }
                                }
                            }
                        }
                    } catch (IllegalArgumentException iae) {
                        AccessDescription ad = AccessDescription.getInstance(nextElement);
                        if (ad != null && ad.getAccessMethod().equals(AccessDescription.id_ad_ocsp)) {
                            GeneralName name = ad.getAccessLocation();
                            if (name.getTagNo() == GeneralName.uniformResourceIdentifier) {
                                return ((DERIA5String) name.getName()).getString();
                            }
                        }
                    }
                }
            } else {
                throw new CertificateNormException("Failed to load CertificatePolicies",
                        CertificateConstants.EP_SOS_WP_3_4_2_CHAPTER_5_4_2);
            }
        }
        return null;
    }

    /**
     *
     * @param cert
     * @return
     * @throws CertificateException
     */
    public String getOCSPFromAuthorityInfoAccess(X509Certificate cert) throws CertificateException {
        ASN1Primitive authInfoAcc = getExtensionValue(cert, CertificateExtention.getAuthorityInfoAccessObjectId());
        if (authInfoAcc != null) {
            if (authInfoAcc instanceof ASN1Sequence) {
                ASN1Sequence seq = (ASN1Sequence) authInfoAcc;
                Enumeration e = seq.getObjects();
                while (e.hasMoreElements()) {
                    Object nextElement = e.nextElement();
                    try {
                        AuthorityInformationAccess authInfoAccess = AuthorityInformationAccess.getInstance(nextElement);
                        if (authInfoAccess != null) {
                            AccessDescription[] ads = authInfoAccess.getAccessDescriptions();
                            for (int i = 0; i < ads.length; i++) {
                                if (ads[i].getAccessMethod().equals(AccessDescription.id_ad_ocsp)) {
                                    GeneralName name = ads[i].getAccessLocation();
                                    if (name.getTagNo() == GeneralName.uniformResourceIdentifier) {
                                        return ((DERIA5String) name.getName()).getString();
                                    }
                                }
                            }
                        }
                    } catch (IllegalArgumentException iae) {
                        AccessDescription ad = AccessDescription.getInstance(nextElement);
                        if (ad != null && ad.getAccessMethod().equals(AccessDescription.id_ad_ocsp)) {
                            GeneralName name = ad.getAccessLocation();
                            if (name.getTagNo() == GeneralName.uniformResourceIdentifier) {
                                return ((DERIA5String) name.getName()).getString();
                            }
                        }
                    }
                }
            } else {
                throw new CertificateNormException("Failed to load CertificatePolicies",
                        EHEALTH_X509_3_1);
            }
        }
        return null;
    }

    /**
     *
     * @param endCert
     * @param crlDistributionPointsEndCert
     * @throws CertificateException
     */
    public void getCRLDistributionPoints(X509Certificate endCert, List<String> crlDistributionPointsEndCert)
            throws CertificateException {
        ASN1Primitive crlDPDERObject = getExtensionValue(endCert, Extension.cRLDistributionPoints.getId());
        if (crlDPDERObject != null) {
            CRLDistPoint crlDistPoints = CRLDistPoint.getInstance(crlDPDERObject);
            if (crlDistPoints != null) {
                DistributionPoint[] distPoints = crlDistPoints.getDistributionPoints();
                for (int i = 0; i < distPoints.length; i++) {
                    DistributionPointName distributionPointName = distPoints[i].getDistributionPoint();
                    if (distributionPointName.getType() == DistributionPointName.FULL_NAME) {
                        GeneralName[] generalNames = GeneralNames.getInstance(distributionPointName.getName()).getNames();
                        for (int j = 0; j < generalNames.length; j++) {
                            if (generalNames[j].getTagNo() == GeneralName.uniformResourceIdentifier) {
                                String url = ((DERIA5String) generalNames[j].getName()).getString();
                                crlDistributionPointsEndCert.add(url);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     *
     * @param po_x509Certificate
     * @param po_warnings
     * @param po_exceptions
     * @throws CertificateException
     */
    public void checkCertificatePolicies(X509Certificate po_x509Certificate, List<CertificateException> po_warnings, List<CertificateException> po_exceptions) throws CertificateException {
        ASN1Primitive certificatePoliciesObject = getExtensionValue(po_x509Certificate,
                Extension.certificatePolicies.getId());
        if (certificatePoliciesObject != null) {
            if (certificatePoliciesObject instanceof ASN1Sequence) {
                ASN1Sequence seq = (ASN1Sequence) certificatePoliciesObject;
                Enumeration e = seq.getObjects();
                while (e.hasMoreElements()) {
                    ASN1Sequence s = ASN1Sequence.getInstance(e.nextElement());
                    if (s.size() > 1) {
                        po_warnings.add(new CertificateNormException(
                                "CertificatePolicies Policyinformation SHOULD only include an OID, no PolicyQualifier.",
                                CertificateConstants.EP_SOS_WP_3_4_2_CHAPTER_5_4_2));
                    }
                }
            } else {
                po_exceptions.add(new CertificateNormException("Failed to load CertificatePolicies",
                        CertificateConstants.EP_SOS_WP_3_4_2_CHAPTER_5_4_2));
            }
        }
    }

    /**
     *
     * @param po_chain
     * @return
     */
    public Boolean setIssuingCaSupportOcsp(List<X509Certificate> po_chain) {
        Boolean ls_issuingCaSupportOcsp = false;
        List<AccessDescription[]> accessDescriptionList = getAccessDescriptions(po_chain.get(1));
        for (AccessDescription[] accessDescriptions : accessDescriptionList) {
            if (accessDescriptions != null) {
                for (AccessDescription accessDescription : accessDescriptions) {
                    if (accessDescription.getAccessMethod().equals(AccessDescription.id_ad_ocsp)) {
                        ls_issuingCaSupportOcsp  = true;
                    }
                }
            }
        }
        return ls_issuingCaSupportOcsp;
    }

    /**
     *
     * @param po_x509Certificate
     * @return
     */
    public Boolean getOCSPPresent(X509Certificate po_x509Certificate) {
        Boolean lb_return = false;
        List<AccessDescription[]> accessDescriptionList = getAccessDescriptions(po_x509Certificate);
        for (AccessDescription[] accessDescriptions : accessDescriptionList) {
            for (AccessDescription accessDescription : accessDescriptions) {
                if (accessDescription.getAccessMethod().equals(AccessDescription.id_ad_ocsp)) {
                    lb_return = true;
                }

            }
        }
        return lb_return;
    }

    /**
     *
     * @param po_x509Certificate
     * @return
     */
    public Boolean getAccessLocationForOcspPresent(X509Certificate po_x509Certificate) {
        Boolean lb_return = false;
        Boolean lb_ocspPresent = false;

        List<AccessDescription[]> accessDescriptionList = getAccessDescriptions(po_x509Certificate);
        for (AccessDescription[] accessDescriptions : accessDescriptionList) {
            for (AccessDescription accessDescription : accessDescriptions) {
                if (accessDescription.getAccessMethod().equals(AccessDescription.id_ad_ocsp)) {
                    lb_ocspPresent = true;
                }
                if (lb_ocspPresent && accessDescription.getAccessLocation() != null) {
                    lb_return = true;
                }

            }
        }
        return lb_return;
    }
}
