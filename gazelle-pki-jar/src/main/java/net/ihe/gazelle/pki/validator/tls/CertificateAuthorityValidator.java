package net.ihe.gazelle.pki.validator.tls;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

import net.ihe.gazelle.pki.model.PKiProvider;
import net.ihe.gazelle.pki.validator.AbstractCertificateValidator;

public class CertificateAuthorityValidator extends AbstractCertificateValidator {

    public CertificateAuthorityValidator(PKiProvider po_pKiProvider) {
        super(po_pKiProvider);
    }

    @Override
    public void validate(List<X509Certificate> chain, List<CertificateException> pkiValidation,
                         List<CertificateException> warnings, boolean revocation) {
        //
    }

}
