package net.ihe.gazelle.pki.model;

import net.ihe.gazelle.pki.CertificateConstants;
import org.apache.commons.lang.Validate;
import org.apache.commons.ssl.PEMItem;
import org.apache.commons.ssl.PEMUtil;

import javax.persistence.Entity;
import java.io.IOException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.util.List;

/**
 * The aim of this class is to have a certificate request build from a CSR in PEM format. By this way, a certificate can be set and signed in the system without having the private key of the requester.
 */
@Entity
public class CertificateRequestCSR extends CertificateRequest {

    /**
     * Init the CertificateRequest object from a CSR in PEM format.
     *
     * @param csr Certificate Signing Request in PEM format
     * @throws CertificateException if unable to load the public key from the csr file.
     */
    public void loadFromCsr(String csr) throws CertificateException {

        final List pemItems = PEMUtil.decode(csr.getBytes(CertificateConstants.UTF_8));

        // Verify list isn't empty - uses Apache Commons Lang.
        Validate.isTrue(!pemItems.isEmpty());

        // No support for generics, so have to cast.
        // (Could have cast the entire List)
        final PEMItem csrPemFormat = (PEMItem) pemItems.get(0);

        // Verify the type.
        Validate.isTrue(csrPemFormat.pemType.equals("CERTIFICATE REQUEST") || csrPemFormat.pemType
                .equals("NEW CERTIFICATE REQUEST"), "This is not a CSR");


        String name = null;
        PublicKey publicKey = null;
        try {
            name = getPKiProvider().getPKCS10CertificationSubjectToString(csrPemFormat);
            publicKey = getPKiProvider().getPublicKeyFromPKCS10(csrPemFormat);
        } catch (IOException e) {
            e.printStackTrace();
        }

        setPublicKey(publicKey);
        setSubject(name);
    }
}
