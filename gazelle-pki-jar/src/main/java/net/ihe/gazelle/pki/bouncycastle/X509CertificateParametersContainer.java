package net.ihe.gazelle.pki.bouncycastle;

import net.ihe.gazelle.pki.model.CertificateRequest;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;

import java.math.BigInteger;
import java.security.PublicKey;

public class X509CertificateParametersContainer  {

    private CertificateRequest certificateRequest;
    private X500Name issuerPrincipal;
    private PublicKey publicKey;
    private BigInteger serialNumber;
    private X500Name subjectPrincipal;
    private int certificateId;

    /**
     *
     * @param certificateRequest
     * @param issuerPrincipal
     * @param publicKey
     * @param serialNumber
     * @param subjectPrincipal
     * @param certificateId
     */
    public X509CertificateParametersContainer(CertificateRequest certificateRequest, X500Name issuerPrincipal,
                                              PublicKey publicKey, BigInteger serialNumber, X500Name subjectPrincipal, int certificateId) {
        this.certificateRequest = certificateRequest;
        this.issuerPrincipal = issuerPrincipal;
        this.publicKey = publicKey;
        this.serialNumber = serialNumber;
        this.subjectPrincipal = subjectPrincipal;
        this.certificateId = certificateId;
    }

    /**
     *
     * @return
     */
    public CertificateRequest getCertificateRequest() {
        return certificateRequest;
    }

    /**
     *
     * @return
     */
    public X500Name getIssuerPrincipal() {
        return issuerPrincipal;
    }

    /**
     *
     * @return
     */
    public PublicKey getPublicKey() {
        return publicKey;
    }

    /**
     *
     * @return
     */
    public BigInteger getSerialNumber() {
        return serialNumber;
    }

    /**
     *
     * @return
     */
    public X500Name getSubjectPrincipal() {
        return subjectPrincipal;
    }

    /**
     *
     * @return
     */
    public int getCertificateId() {
        return certificateId;
    }

    /**
     *
     * @return
     */
    public SubjectPublicKeyInfo getSubjectPublicKeyInfo() {
        return SubjectPublicKeyInfo.getInstance(publicKey.getEncoded());
    }

}
