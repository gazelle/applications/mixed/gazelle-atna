package net.ihe.gazelle.pki;

import net.ihe.gazelle.pki.bouncycastle.CertificateBC;
import net.ihe.gazelle.pki.model.PKiProvider;
import org.apache.poi.util.IOUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.security.KeyPair;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.List;

/**
 * Helper for PEM imports into java.security objects
 * Please use this class everytime it's possible to avoid declaring dependencies to bouncycastle everywhere.
 *
 * @author ceoche
 */
public class PKIXImporter {
    protected PKiProvider mo_pKiProvider = new CertificateBC();

    /**
     * Getter of attribute mo_pkiProvider.
     * @return the Instance of PKIProvider.
     */
    protected PKiProvider getPKiProvider() {
        return mo_pKiProvider;
    }

    /**
     *
     * @param pemData
     * @return
     * @throws IOException
     * @throws CertificateException
     */
    public List<X509Certificate> getX509CertificatesListFromPEM(byte[] pemData) throws IOException, CertificateException {

        if (pemData != null & pemData.length > 0) {

            List<X509Certificate> certificates;
            Reader reader = new InputStreamReader(new ByteArrayInputStream(pemData), CertificateConstants.UTF_8);

            try {
                certificates = getPKiProvider().getCertificatesFromPemObject(reader);
            } finally {
                IOUtils.closeQuietly(reader);
            }

            if (certificates.isEmpty()) {
                throw new CertificateException("No certificates found in the given PEM file");
            }
            return certificates;

        } else {
            throw new IOException("PEM file is empty");
        }

    }

    /**
     *
     * @param pemData
     * @return
     * @throws IOException
     * @throws CertificateException
     */
    public X509Certificate getUniqueX509CertificateFromPEM(byte[] pemData) throws IOException, CertificateException {
        List<X509Certificate> certificates = getX509CertificatesListFromPEM(pemData);
        if (certificates.size() > 1) {
            throw new NotUniqueCertificateException("Several certificates have been found in the given PEM file");
        } else {
            return certificates.get(0);
        }
    }

    /**
     *
     * @param pemData
     * @param password
     * @return
     * @throws IOException
     */
    public KeyPair getKeyPairFromPEM(byte[] pemData, String password) throws IOException {
        if (pemData.length > 0) {

            Reader reader = null;
            KeyPair key = null;

            if (password == null) {
                password = "";
            }

            try {
                reader = new InputStreamReader(new ByteArrayInputStream(pemData), StandardCharsets.UTF_8);

                return getPKiProvider().getKeyPairFromPEM(reader, password);

            } finally {
                IOUtils.closeQuietly(reader);
            }

        } else {
            throw new IOException("Empty key file");
        }
    }

}
