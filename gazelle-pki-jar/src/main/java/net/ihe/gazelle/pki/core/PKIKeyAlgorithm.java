package net.ihe.gazelle.pki.core;

public enum PKIKeyAlgorithm {

    RSA("RSA", 1024, 2048, 3072, 4096),
    ECDSA("ECDSA", 160, 256, 512);


    private String providerLabel;
    private int[] keyLength;

    PKIKeyAlgorithm(String providerLabel, int... allowedLength) {
        this.providerLabel = providerLabel;
        this.keyLength = allowedLength;
    }

    }
