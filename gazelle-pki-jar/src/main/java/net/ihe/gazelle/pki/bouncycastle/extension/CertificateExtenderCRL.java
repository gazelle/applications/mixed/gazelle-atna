package net.ihe.gazelle.pki.bouncycastle.extension;

import net.ihe.gazelle.pki.CertificateExtender;
import net.ihe.gazelle.pki.bouncycastle.X509CertificateParametersContainer;
import net.ihe.gazelle.pki.bouncycastle.CertificateBuilder;
import net.ihe.gazelle.preferences.PreferenceService;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

public class CertificateExtenderCRL extends CertificateExtenderSAN implements CertificateExtender {

    @Override
    public void addExtension(CertificateBuilder certGen, X509CertificateParametersContainer parameters)
            throws CertificateException {
        super.addExtension(certGen, parameters);

        Integer caId = 0;
        if (parameters != null) {
            if ((parameters.getCertificateRequest() != null) &&
                    (parameters.getCertificateRequest().getCertificateAuthority() != null)) {
                caId = parameters.getCertificateRequest().getCertificateAuthority().getId();
            } else {
                caId = parameters.getCertificateId();
            }
        }

        String crlUrl = generateCrlUrl(caId);


        certGen.addCRLExtension(crlUrl, true);
    }

    @Override
    public void modifyCertificate(X509Certificate cert, X509CertificateParametersContainer parameters)
            throws CertificateException {
        // nothing
    }

    private String generateCrlUrl(Integer caId) {
        String crlBase = PreferenceService.getString("crl_url");
        if (!crlBase.endsWith("/")) {
            crlBase = crlBase + "/";
        }
        String url = crlBase + "crl/" + Integer.toString(caId) + "/cacrl.crl";
        return url;
    }

}
