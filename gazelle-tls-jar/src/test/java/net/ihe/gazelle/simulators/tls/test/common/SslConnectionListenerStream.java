package net.ihe.gazelle.simulators.tls.test.common;

import java.io.PrintStream;

import net.ihe.gazelle.proxy.model.message.AbstractMessage;
import net.ihe.gazelle.simulators.tls.TlsConnectionListener;
import net.ihe.gazelle.simulators.tls.model.TlsConnection;

public class SslConnectionListenerStream implements TlsConnectionListener {

    private PrintStream stream;

    private String prefix;

    private TlsConnection lastConnection;

    public SslConnectionListenerStream(String prefix, PrintStream out) {
        super();
        this.prefix = prefix;
        this.stream = out;
    }

    public TlsConnection getLastConnection() {
        return lastConnection;
    }

    private void printPrefix() {
        stream.print(prefix);
        stream.print(Thread.currentThread().getId() + " -");
    }

    @Override
    public void logConnection(TlsConnection connection, int channelId) {
        this.lastConnection = connection;
        printPrefix();
        stream.println(connection.isSuccess());
        printPrefix();
        stream.println(connection.getExceptionMessage());
        printPrefix();
        stream.println(connection.getProtocol() + " - " + connection.getCipherSuite());
        // if (event.isSuccess()) {
        // printPrefix();
        // stream.println("**  SUCCESS  **");
        // } else {
        // printPrefix();
        // stream.println("**  FAILURE  **");
        // printPrefix();
        // stream.println(event.getExceptionMessage());
        // printPrefix();
        // stream.println(event.getExceptionStackTrace());
        // }
        // if (event.getSession() != null) {
        // printPrefix();
        // stream.println("** Session : ");
        // printPrefix();
        // stream.println(event.getSession().toString());
        // } else {
        // stream.println("** null session");
        // }
        // printPrefix();
        // stream.println("***************");
    }

    public void onMessage(AbstractMessage message) {
        printPrefix();
        stream.println("new message");
        printPrefix();
        stream.println(message.toString());
    }

}
