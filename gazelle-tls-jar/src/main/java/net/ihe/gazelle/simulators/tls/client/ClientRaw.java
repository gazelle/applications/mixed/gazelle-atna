package net.ihe.gazelle.simulators.tls.client;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.proxy.netty.model.enums.ChannelType;
import net.ihe.gazelle.proxy.netty.Proxy;
import net.ihe.gazelle.simulators.tls.client.parameters.ClientParameter;
import net.ihe.gazelle.simulators.tls.client.parameters.ClientParameterText;

public class ClientRaw extends ClientSocket {

    private ClientParameterText message = new ClientParameterText("Message", "Hello");

    @Override
    public void writeInSocket(OutputStream outputStream, Proxy<?, ?> proxyToUse) throws IOException {
        OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8");
        writer.write(message.getValueAsString());
        writer.flush();
    }

    @Override
    public List<ClientParameter> getParameters() {
        List<ClientParameter> result = new ArrayList<ClientParameter>(1);
        result.add(message);
        return result;
    }

    @Override
    public ChannelType getChannelType() {
        return ChannelType.RAW;
    }

}
