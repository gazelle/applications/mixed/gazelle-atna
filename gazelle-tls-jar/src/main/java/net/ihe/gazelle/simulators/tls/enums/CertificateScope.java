package net.ihe.gazelle.simulators.tls.enums;

import net.ihe.gazelle.common.jsf.Labelable;

import java.io.Serializable;

public enum CertificateScope implements Serializable, Labelable {

    PKI,
    CUSTOM;

    @Override
    public String getDisplayLabel() {
        return this.name();
    }

}
