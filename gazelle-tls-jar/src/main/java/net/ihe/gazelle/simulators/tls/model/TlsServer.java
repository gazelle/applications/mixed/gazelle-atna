package net.ihe.gazelle.simulators.tls.model;

import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.proxy.listeners.HttpEventListener;
import net.ihe.gazelle.proxy.netty.model.enums.ChannelType;
import net.ihe.gazelle.proxy.netty.ConnectionConfig;
import net.ihe.gazelle.proxy.netty.Proxy;
import net.ihe.gazelle.proxy.netty.protocols.http.HttpProxy;
import net.ihe.gazelle.simulators.tls.TlsConnectionListener;
import net.ihe.gazelle.simulators.tls.client.Client;
import net.ihe.gazelle.simulators.tls.common.TlsProxyConnectionConfigAsServer;
import net.ihe.gazelle.simulators.tls.common.TlsSimulatorException;
import net.ihe.gazelle.simulators.tls.enums.SimulatorType;

import javax.net.ssl.SSLParameters;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.Collections;
import java.util.logging.Logger;

@Entity
@DiscriminatorValue("Server")
public class TlsServer extends TlsSimulator {

    private static final long serialVersionUID = 4866424928812641378L;

    private static final String HTTP = "HTTP";
    private static final Integer HTTP_DEFAULT_CONTENT_SIZE = 16;

    @Column(name = "is_need_client_auth")
    private boolean needClientAuth;

    @Column(name = "remote_host")
    private String remoteHost;

    @Column(name = "remote_port")
    private Integer remotePort;

    @Column(name = "local_port")
    private Integer localPort;

    @Column(name = "channel_type")
    private ChannelType channelType;

    @Column(name = "is_running")
    private boolean running;

    public String getRemoteHost() {
        return remoteHost;
    }

    public void setRemoteHost(String previousHost) {
        this.remoteHost = previousHost;
    }

    public Integer getRemotePort() {
        return remotePort;
    }

    public void setRemotePort(Integer previousPort) {
        this.remotePort = previousPort;
    }

    public boolean isNeedClientAuth() {
        return needClientAuth;
    }

    public void setNeedClientAuth(boolean needClientAuth) {
        this.needClientAuth = needClientAuth;
    }

    public ChannelType getChannelType() {
        return channelType;
    }

    public void setChannelType(ChannelType previousType) {
        this.channelType = previousType;
    }

    public TlsServer() {
        super();
        this.channelType = ChannelType.RAW;
    }

    @Override
    public SimulatorType getType() {
        return SimulatorType.SERVER;
    }

    @Override
    public TlsServer clone() {
        TlsServer copy = (TlsServer) super.clone();
        copy.setNeedClientAuth(this.isNeedClientAuth());
        copy.setRemoteHost(this.getRemoteHost());
        copy.setRemotePort(this.getRemotePort());
        copy.setLocalPort(this.getLocalPort());
        copy.setChannelType(this.getChannelType());
        copy.setRunning(false);
        return copy;
    }

    @Override
    protected TlsServer newInstance() {
        return new TlsServer();
    }

    @Override
    protected void updateParameters(SSLParameters sslParameters) {
        sslParameters.setNeedClientAuth(needClientAuth);
    }

    public Proxy<?, ?> start(TlsConnectionListener connectionListener) throws TlsSimulatorException {
        ConnectionConfig connectionConfig = new TlsProxyConnectionConfigAsServer(this,
                Collections.singletonList(connectionListener), getLocalPort(), getRemoteHost(), getRemotePort(),
                getChannelType());
        Proxy<?, ?> proxy = channelType.getProxy(connectionConfig);
        proxy = getProxyTypeServer(proxy, connectionConfig);
        proxy.start();
        if (!proxy.iscException()) {
            return proxy;
        } else {
            return null;
        }

    }

    public Integer getLocalPort() {
        return localPort;
    }

    public void setLocalPort(Integer localPort) {
        this.localPort = localPort;
    }

    public boolean isRunning() {
        return running;
    }

    public void setRunning(boolean isRunning) {
        this.running = isRunning;
    }

    public static String getSimulatorKeywordPrefix() {
        return "server_";
    }

    @Override
    public void executeTest(TlsConnectionListener tlsTestInstanceCallbacker, int testCaseId, String sutHost,
                            int sutPort, Client applicationMsg) throws TlsSimulatorException {
        // TODO Auto-generated method stub

    }

    /**
     * Allows to set the max content size for the HTTP proxy type
     *
     * @param proxy            Proxy server element
     * @param connectionConfig ConnectionConfig element
     * @return proxy
     */
    private Proxy getProxyTypeServer(Proxy proxy, ConnectionConfig connectionConfig) {
        if (getChannelType().getIdentifier() == HTTP) {
            HttpEventListener proxyEventListener = new HttpEventListener();
            HttpProxy httpProxy = new HttpProxy(proxyEventListener, connectionConfig);
            httpProxy.setHttpMaxContentSize(getHttpContentMaxSize());
            proxy = httpProxy;
        }
        return proxy;
    }

    /**
     * Get the max http content size from application configuration
     *
     * @return max http content size
     */
    private Integer getHttpContentMaxSize() {
        try {
            Integer maxSize = Integer.parseInt(PreferenceService.getString("http_max_content_size"));
            if (maxSize == null) {
                return HTTP_DEFAULT_CONTENT_SIZE;
            } else {
                return maxSize;
            }
        } catch (Exception e) {
            Logger.getLogger(e.getMessage());
            return HTTP_DEFAULT_CONTENT_SIZE;
        }
    }
}
