package net.ihe.gazelle.simulators.tls.client;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.ihe.gazelle.proxy.netty.model.enums.ChannelType;
import net.ihe.gazelle.proxy.netty.Proxy;
import net.ihe.gazelle.simulators.tls.client.parameters.ClientParameter;
import net.ihe.gazelle.simulators.tls.client.parameters.ClientParameterInteger;
import net.ihe.gazelle.simulators.tls.client.parameters.ClientParameterString;
import net.ihe.gazelle.simulators.tls.client.parameters.ClientParameterText;

import org.apache.log4j.Logger;
import org.openhealthtools.openatna.syslog.SyslogException;
import org.openhealthtools.openatna.syslog.message.StringLogMessage;
import org.openhealthtools.openatna.syslog.protocol.ProtocolMessage;
import org.openhealthtools.openatna.syslog.protocol.ProtocolMessageFactory;

public class ClientSyslog extends ClientSocket {

    private static final Logger log = Logger.getLogger(ClientSyslog.class);
    private List<ClientParameter> parameters;
    private ClientParameterInteger facility;
    private ClientParameterInteger severity;
    private ClientParameterString timestamp;
    private ClientParameterString host;
    private ClientParameterText payload;
    private ClientParameterString appName;
    private ClientParameterString messageId;
    private ClientParameterString procId;

    public ClientSyslog() {
        super();
        parameters = new ArrayList<ClientParameter>();

        facility = new ClientParameterInteger("Facility", 10);
        severity = new ClientParameterInteger("Severity", 5);
        timestamp = new ClientParameterString("Timestamp", ProtocolMessageFactory.formatDate(new Date()));
        host = new ClientParameterString("Host", "TLStest");
        appName = new ClientParameterString("Application name", "IHE_XDS");
        messageId = new ClientParameterString("Message id", "ATNALOG");
        procId = new ClientParameterString("Proc id", "1234");
        payload = new ClientParameterText("Payload", "<atna></atna>");

        parameters.add(facility);
        parameters.add(severity);
        parameters.add(timestamp);
        parameters.add(host);
        parameters.add(appName);
        parameters.add(messageId);
        parameters.add(procId);
        parameters.add(payload);
    }

    @Override
    public void writeInSocket(OutputStream outputStream, Proxy<?, ?> proxyToUse) throws IOException {
        try {
            ProtocolMessage<String> sl = new ProtocolMessage<String>(facility.getValue(), severity.getValue(),
                    timestamp.getValueAsString(), host.getValueAsString(), new StringLogMessage(
                    payload.getValueAsString()), appName.getValueAsString(), messageId.getValueAsString(),
                    procId.getValueAsString());
            byte[] bytes = sl.toByteArray();

            outputStream.write(Integer.toString(bytes.length).getBytes());
            outputStream.write(" ".getBytes());
            outputStream.write(bytes);

        } catch (SyslogException e) {
            log.error(e);
        }
    }

    @Override
    public List<ClientParameter> getParameters() {
        return parameters;
    }

    @Override
    public ChannelType getChannelType() {
        return ChannelType.SYSLOG;
    }

}
