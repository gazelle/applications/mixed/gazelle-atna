package net.ihe.gazelle.simulators.tls.model;

import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Lob;

import net.ihe.gazelle.pki.CertificateUtil;
import net.ihe.gazelle.simulators.tls.enums.TlsConnectionDataItemType;

import org.hibernate.annotations.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Entity
@DiscriminatorValue("CHECKTRUSTED")
public class TlsConnectionDataItemCheckTrusted extends TlsConnectionDataItem {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private static Logger log = LoggerFactory.getLogger(TlsConnectionDataItemCheckTrusted.class);

    @Lob
    @Type(type = "text")
    @Column(name = "pem_chain")
    private String pemChain;

    @Lob
    @Type(type = "text")
    @Column(name = "auth_type")
    private String authType;

    public TlsConnectionDataItemCheckTrusted() {
        super();
    }

    public TlsConnectionDataItemCheckTrusted(X509Certificate[] chain, String authType,
                                             TlsConnectionDataItemType tlsConnectionDataItemType) {
        super();
        init(tlsConnectionDataItemType);
        try {
            pemChain = CertificateUtil.getPEM(chain);
        } catch (CertificateException e) {
            log.error("Failed to transform certificates to PEM");
            pemChain = null;
        }
        this.authType = authType;
    }

    public String getPemChain() {
        return pemChain;
    }

    public void setPemChain(String pemChain) {
        this.pemChain = pemChain;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    @Override
    public boolean isAliases() {
        return false;
    }

}
