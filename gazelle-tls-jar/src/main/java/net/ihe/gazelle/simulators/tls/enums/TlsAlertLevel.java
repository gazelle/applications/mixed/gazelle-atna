package net.ihe.gazelle.simulators.tls.enums;

import net.ihe.gazelle.common.jsf.Labelable;

import java.io.Serializable;

public enum TlsAlertLevel implements Labelable, Serializable {

    WARNING("warning", 1),
    FATAL("fatal", 2);

    private String label;
    private int value;

    TlsAlertLevel(String label, int value) {
        this.label = label;
        this.value = value;
    }

    public String toString() {
        return label;
    }

    public String getLabel() {
        return label;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String getDisplayLabel() {
        return getLabel();
    }
}
