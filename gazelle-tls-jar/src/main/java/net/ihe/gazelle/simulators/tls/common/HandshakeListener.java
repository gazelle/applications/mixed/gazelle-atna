package net.ihe.gazelle.simulators.tls.common;

import java.net.SocketAddress;

import org.jboss.netty.channel.ChannelFuture;
import org.jboss.netty.channel.ChannelFutureListener;
import org.jboss.netty.handler.ssl.SslHandler;

public class HandshakeListener implements ChannelFutureListener {

    private TlsListener listener;
    private SslHandler sslHandler;
    private int renegociationCounter;

    public HandshakeListener(TlsListener listener, int renegociationCounter, SslHandler sslHandler) {
        super();
        this.listener = listener;
        this.renegociationCounter = renegociationCounter;
        this.sslHandler = sslHandler;
    }

    @Override
    public void operationComplete(ChannelFuture future) throws Exception {
        SocketAddress localAddress = future.getChannel().getLocalAddress();
        SocketAddress remoteAddress = future.getChannel().getRemoteAddress();

        this.listener.event(future.isSuccess(), future.getCause(), renegociationCounter, sslHandler.getEngine(),
                localAddress, remoteAddress, future.getChannel().getId());
    }
}