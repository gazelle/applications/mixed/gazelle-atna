package net.ihe.gazelle.simulators.tls.test.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.pki.enums.ContextType;

@Entity
@Table(name = "tls_test_suite", schema = "public")
@SequenceGenerator(name = "tls_test_suite_sequence", sequenceName = "tls_test_suite_id_seq", allocationSize = 1)
public class TlsTestSuite {

    @Id
    @GeneratedValue(generator = "tls_test_suite_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private int id;

    @Column(name = "name", unique = true, nullable = false)
    private String name;

    @Column(name = "context")
    private ContextType context;

    @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.LAZY)
    private List<TlsTestCase> testCases;

    public int getId() {
        return id;
    }

    @FilterLabel
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public ContextType getContext() {
        return context;
    }

    public void setContext(ContextType context) {
        this.context = context;
    }

    public List<TlsTestCase> getTestCases() {
        return testCases;
    }

    public void setTestCases(List<TlsTestCase> testCases) {
        this.testCases = testCases;
    }


}
