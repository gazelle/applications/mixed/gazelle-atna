package net.ihe.gazelle.simulators.tls.test.model;

import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.pki.enums.ContextType;
import net.ihe.gazelle.simulators.tls.enums.SimulatorType;
import net.ihe.gazelle.simulators.tls.enums.TlsAlertDescription;
import net.ihe.gazelle.simulators.tls.enums.TlsAlertLevel;
import net.ihe.gazelle.simulators.tls.model.TlsSimulator;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "tls_test_case", schema = "public")
@SequenceGenerator(name = "tls_test_case_sequence", sequenceName = "tls_test_case_id_seq", allocationSize = 1)
public class TlsTestCase extends AuditedObject implements Serializable {

    @Id
    @GeneratedValue(generator = "tls_test_case_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private int id;

    @Column(name = "name", unique = true, nullable = false)
    private String name;

    @Column(name = "description", length = 1023)
    private String description;

    @Column(name = "context")
    private ContextType context;

    @ManyToOne(cascade = {CascadeType.REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = "dataSet_id")
    private TlsTestDataSet tlsDataSet;

    @Column(name = "suttype")
    private SimulatorType sutType;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = "simulator_id")
    private TlsSimulator simulator;

    @Column(name = "expected_handshake_success")
    private boolean expectedHandshakeSuccess;

    @Column(name = "mandatory_handshake_success")
    private boolean mandatoryHandshakeSuccess;

    @Column(name = "expected_alert_level")
    private TlsAlertLevel expectedAlertLevel;

    @Column(name = "mandatory_alert_level")
    private boolean mandatoryAlertLevel;

    @ElementCollection
    @CollectionTable(name="tls_test_case_expectedalertdescriptions", joinColumns = @JoinColumn(name = "tls_test_case_id"))
    @Column(name="element")
    private Set<TlsAlertDescription> expectedAlertDescriptions;

    @Column(name = "mandatory_alert_description")
    private boolean mandatoryAlertDescription;

    public TlsTestCase() {
        this.expectedHandshakeSuccess = true;
        this.mandatoryHandshakeSuccess = true;
        this.mandatoryAlertLevel = false;
        this.mandatoryAlertDescription = false;
    }

    public int getId() {
        return id;
    }

    @FilterLabel
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public ContextType getContext() {
        return context;
    }

    public void setContext(ContextType context) {
        this.context = context;
    }

    public SimulatorType getSutType() {
        return sutType;
    }

    public void setSutType(SimulatorType simulatorType) {
        this.sutType = simulatorType;
    }

    public TlsSimulator getSimulator() {
        return simulator;
    }

    public String getSimulatorKeyword() {
        if (simulator == null) {
            return "None";
        } else {
            return simulator.getKeyword();
        }
    }

    public void setSimulator(TlsSimulator simulator) {
        this.simulator = simulator;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TlsTestDataSet getTlsDataSet() {
        return tlsDataSet;
    }

    public void setTlsDataSet(TlsTestDataSet tlsDataSet) {
        this.tlsDataSet = tlsDataSet;
    }

    public boolean isExpectedHandshakeSuccess() {
        return expectedHandshakeSuccess;
    }

    public void setExpectedHandshakeSuccess(boolean expectedHandshakeSuccess) {
        this.expectedHandshakeSuccess = expectedHandshakeSuccess;
    }

    public boolean isMandatoryHandshakeSuccess() {
        return mandatoryHandshakeSuccess;
    }

    public void setMandatoryHandshakeSuccess(boolean mandatoryHandshakeSuccess) {
        this.mandatoryHandshakeSuccess = mandatoryHandshakeSuccess;
    }

    public TlsAlertLevel getExpectedAlertLevel() {
        return expectedAlertLevel;
    }

    public void setExpectedAlertLevel(TlsAlertLevel expectedAlertLevel) {
        this.expectedAlertLevel = expectedAlertLevel;
    }

    public boolean isMandatoryAlertLevel() {
        return mandatoryAlertLevel;
    }

    public void setMandatoryAlertLevel(boolean mandatoryAlertLevel) {
        this.mandatoryAlertLevel = mandatoryAlertLevel;
    }

    public Set<TlsAlertDescription> getExpectedAlertDescriptions() {
        return expectedAlertDescriptions;
    }

    public void setExpectedAlertDescriptions(Set<TlsAlertDescription> expectedAlertDescriptions) {
        this.expectedAlertDescriptions = expectedAlertDescriptions;
    }

    public boolean isMandatoryAlertDescription() {
        return mandatoryAlertDescription;
    }

    public void setMandatoryAlertDescription(boolean mandatoryAlertDescription) {
        this.mandatoryAlertDescription = mandatoryAlertDescription;
    }


}
