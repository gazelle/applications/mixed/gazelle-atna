package net.ihe.gazelle.simulators.tls.client.parameters;

public class ClientParameterString implements ClientParameter {

    private String value;
    private String label;

    public ClientParameterString(String label, String initialValue) {
        super();
        this.label = label;
        this.value = initialValue;
    }

    @Override
    public String getValueAsString() {
        return value;
    }

    @Override
    public void setValueAsString(String value) {
        this.value = value;
    }

    @Override
    public String getLabel() {
        return label;
    }

    @Override
    public int getType() {
        return 0;
    }

}
