package net.ihe.gazelle.simulators.tls;

import net.ihe.gazelle.simulators.tls.client.Client;
import net.ihe.gazelle.simulators.tls.client.ClientDicomEcho;
import net.ihe.gazelle.simulators.tls.client.ClientHL7;
import net.ihe.gazelle.simulators.tls.client.ClientRaw;
import net.ihe.gazelle.simulators.tls.client.ClientSyslog;
import net.ihe.gazelle.simulators.tls.client.ClientWebserviceSimple;

public enum ClientType {

    // HTTP_SIMPLE(ClientHTTPSimple.class),

    WEBSERVICE(ClientWebserviceSimple.class, "WS"),

    DICOM_ECHO(ClientDicomEcho.class, "DICOM"),

    HL7(ClientHL7.class, "HL7"),

    SYSLOG(ClientSyslog.class, "SYSLOG"),

    RAW(ClientRaw.class, "RAW");

    private Class<? extends Client> clientClass;
    private String paramName;

    ClientType(Class<? extends Client> clientClass, String paramName) {
        this.clientClass = clientClass;
        this.paramName = paramName;
    }

    public Client newClient() {
        try {
            return clientClass.newInstance();
        } catch (Exception e) {
            throw new RuntimeException("Failed to create client for type " + this);
        }
    }

    public String getParamName() {
        return this.paramName;
    }

    public String toString() {
        return this.getParamName();
    }

}
