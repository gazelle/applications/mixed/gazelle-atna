package net.ihe.gazelle.simulators.tls.enums;

public enum TestVerdictType {
    NOT_RUN,
    PASSED,
    FAILED,
}
