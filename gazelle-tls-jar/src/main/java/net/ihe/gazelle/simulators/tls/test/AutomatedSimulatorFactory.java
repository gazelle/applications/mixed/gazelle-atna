package net.ihe.gazelle.simulators.tls.test;

import net.ihe.gazelle.simulators.tls.common.TlsSimulatorKeywordGenerator;
import net.ihe.gazelle.simulators.tls.model.TlsSimulator;
import net.ihe.gazelle.simulators.tls.model.TlsClient;
import net.ihe.gazelle.simulators.tls.test.model.TlsTestCase;

/**
 * Created by cel on 09/10/15.
 */
public class AutomatedSimulatorFactory {

    public static TlsSimulator newSimulator(TlsTestCase testCase) {

        //	If the SUT is a SERVER, a CLIENT simulator must be set up.
        //	If the SUT is a CLIENT, a SERVER simulator must be set up.
        switch (testCase.getSutType()) {
            case SERVER:
                TlsClient simuClient = new TlsClient();
                simuClient.setKeyword(
                        TlsSimulatorKeywordGenerator.getValidRandomKeyword(
                                "automated_" + TlsClient.getSimulatorKeywordPrefix()));
                TlsTestDataSetLoader.loadDataSet(simuClient, testCase.getTlsDataSet());
                return simuClient;
            case CLIENT:
                //TODO affect new server simulator
                return null;
            default:
                return null;
        }
    }
}
