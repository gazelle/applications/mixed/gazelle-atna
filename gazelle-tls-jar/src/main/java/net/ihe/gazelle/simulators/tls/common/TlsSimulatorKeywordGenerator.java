package net.ihe.gazelle.simulators.tls.common;

import net.ihe.gazelle.simulators.tls.model.TlsSimulatorQuery;
import net.ihe.gazelle.simulators.tls.model.TlsClient;
import net.ihe.gazelle.simulators.tls.model.TlsServer;
import org.apache.commons.lang.StringUtils;

import java.util.concurrent.ThreadLocalRandom;


/**
 * Created by cel on 22/09/15.
 */
public class TlsSimulatorKeywordGenerator {

    private static final int MIN_RANGE = 0;
    private static final int MAX_RANGE = 99999;
    private static final int DIGIT_NUMBER = 5;

    public static String getValidRandomKeyword(String prefix) {
        String keyword;

        do {
            keyword = generateKeyword(prefix);
        } while (isKeywordExist(keyword));

        return keyword;
    }

    public static String getValidRandomKeyword(Class entityClass) {
        String prefix;

        if(entityClass.equals(TlsClient.class)) {
            prefix = TlsClient.getSimulatorKeywordPrefix();
        } else if(entityClass.equals(TlsServer.class)){
            prefix = TlsServer.getSimulatorKeywordPrefix();
        } else {
            prefix = entityClass.getName() + "_" ;
        }

        return getValidRandomKeyword(prefix);
    }

    private static String generateKeyword(String prefix) {
        String id, keyword;

        id = Integer.toString(randInt(MIN_RANGE, MAX_RANGE));
        id = StringUtils.leftPad(id, DIGIT_NUMBER, '0');
        keyword = prefix + id ;
        return keyword;
    }

    private static int randInt(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }

    private static boolean isKeywordExist(String keyword) {
        TlsSimulatorQuery query = new TlsSimulatorQuery();
        query.keyword().eq(keyword);
        return query.getCount() > 0;
    }

}
