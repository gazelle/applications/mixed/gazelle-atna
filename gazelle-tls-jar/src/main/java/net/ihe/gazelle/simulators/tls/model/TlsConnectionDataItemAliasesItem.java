package net.ihe.gazelle.simulators.tls.model;

import org.hibernate.annotations.Type;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tls_connection_data_item_aliases_item", schema = "public")
@SequenceGenerator(name = "tls_connection_data_item_aliases_item_sequence", sequenceName = "tls_connection_data_item_aliases_item_id_seq", allocationSize = 1)
public class TlsConnectionDataItemAliasesItem {

    @Id
    @GeneratedValue(generator = "tls_connection_data_item_aliases_item_sequence", strategy = GenerationType.SEQUENCE)
    private int id;

    @Lob
    @Type(type = "text")
    private String value;

    public TlsConnectionDataItemAliasesItem() {
        super();
    }

    public TlsConnectionDataItemAliasesItem(String value) {
        super();
        this.value = value;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
