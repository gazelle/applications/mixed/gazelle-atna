package net.ihe.gazelle.simulators.tls.client.parameters;

public interface ClientParameter {

    int getType();

    String getLabel();

    String getValueAsString();

    void setValueAsString(String value);

}
