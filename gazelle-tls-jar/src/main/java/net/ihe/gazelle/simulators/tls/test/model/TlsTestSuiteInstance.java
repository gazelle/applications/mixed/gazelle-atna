package net.ihe.gazelle.simulators.tls.test.model;

import net.ihe.gazelle.simulators.tls.client.Client;
import net.ihe.gazelle.simulators.tls.common.TlsSimulatorException;
import net.ihe.gazelle.simulators.tls.enums.TestVerdictType;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "tls_test_suite_instance", schema = "public")
@SequenceGenerator(name = "tls_test_suite_instance_sequence", sequenceName = "tls_test_suite_instance_id_seq", allocationSize = 1)
public class TlsTestSuiteInstance extends AuditedObject {

    @Id
    @GeneratedValue(generator = "tls_test_suite_instance_sequence", strategy = GenerationType.SEQUENCE)
    @Column(name = "id", nullable = false, unique = true)
    private int id;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = "test_suite_id")
    private TlsTestSuite testSuite;

    @Column(name = "sut_host")
    private String sutHost;

    @Column(name = "sut_port")
    private int sutPort;

    @Column(name = "test_verdict")
    private TestVerdictType suiteVerdict;

    @OneToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH}, fetch = FetchType.LAZY)
    private List<TlsTestInstance> testInstances;

    public TlsTestSuiteInstance() {
        this.suiteVerdict = TestVerdictType.NOT_RUN;
        this.testInstances = new ArrayList<TlsTestInstance>();
    }

    /**
     * TlsTestSuiteInstance constructor initialized a new instance.
     *
     * @param tlsTestSuite The test suite of this instance.
     * @param sutHost      The host of the SUT
     * @param sutPort      The port of the SUT
     */
    public TlsTestSuiteInstance(TlsTestSuite tlsTestSuite, String sutHost, int sutPort) {
        this.suiteVerdict = TestVerdictType.NOT_RUN;
        this.testInstances = new ArrayList<TlsTestInstance>();
        this.testSuite = tlsTestSuite;
        this.sutHost = sutHost;
        this.sutPort = sutPort;
    }

    public int getId() {
        return id;
    }

    public TlsTestSuite getTestSuite() {
        return testSuite;
    }

    public int getTestSuiteId() {
        return testSuite.getId();
    }

    public String getSutHost() {
        return sutHost;
    }

    public int getSutPort() {
        return sutPort;
    }

    public TestVerdictType getSuiteVerdict() {
        return suiteVerdict;
    }

    public List<TlsTestInstance> getTestInstances() {
        return testInstances;
    }

    public void setTestInstances(List<TlsTestInstance> testInstances) {
        this.testInstances = testInstances;
    }

    /**
     * This method to run the test suite instance. It should be called only once per testSuiteInstance.
     *
     * @param applicationMsg client message (HL7, RAW, etc) to transmit. Can be null if simulator is a server.
     * @throws TlsSimulatorException if unable to set up TLS Context
     */
    public void execute(Client applicationMsg) throws TlsSimulatorException {
        for (TlsTestCase testCase : testSuite.getTestCases()) {
            TlsTestInstance testInstance = new TlsTestInstance(testCase, sutHost, sutPort);
            testInstance.execute(testCase.getSimulator(), applicationMsg);
            testInstances.add(testInstance);
        }
        analyze();
    }

    /**
     * Calculate the test suite verdict. It goes trough all test instances, if one of them is not PASSED, then the test suite verdict is set to FAILED.
     */
    private void analyze() {
        if (!testInstances.isEmpty()) {
            boolean result = true;
            for (TlsTestInstance instance : testInstances) {
                result = result && (instance.getTestVerdict() == TestVerdictType.PASSED);
            }
            if (result == true) {
                suiteVerdict = TestVerdictType.PASSED;
            } else {
                suiteVerdict = TestVerdictType.FAILED;
            }
        }
    }

}
