package net.ihe.gazelle.xua.model;

import org.jboss.seam.annotations.Name;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Created by aberge on 23/05/17.
 */
@Entity
@Name("timestamp")
@DiscriminatorValue("timestamp")
public class Timestamp extends SOAPHeaderPart {

    // in seconds
    @Column(name = "created_offset")
    private Integer createdOffset;

    // in seconds
    @Column(name = "duration")
    private Integer duration;

    public Timestamp(){
        super();
        createdOffset = 0;
        duration = 3600;
    }

    @Override
    public String getType() {
        return "Timestamp";
    }

    public Integer getCreatedOffset() {
        return createdOffset;
    }

    public void setCreatedOffset(Integer createdOffset) {
        this.createdOffset = createdOffset;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Timestamp)) {
            return false;
        }

        Timestamp timestamp = (Timestamp) o;

        if (createdOffset != null ? !createdOffset.equals(timestamp.createdOffset) : timestamp.createdOffset != null) {
            return false;
        }
        return duration != null ? duration.equals(timestamp.duration) : timestamp.duration == null;
    }

    @Override
    public int hashCode() {
        int result = createdOffset != null ? createdOffset.hashCode() : 0;
        result = 31 * result + (duration != null ? duration.hashCode() : 0);
        return result;
    }
}
