package net.ihe.gazelle.xua.model;

/**
 * <p>TestStatus enum.</p>
 *
 * @author aberge
 * @version 1.0: 24/10/17
 */
public enum TestStatus {
    PASSED,
    FAILED,
    ABORTED,
    UNKNOWN
}
