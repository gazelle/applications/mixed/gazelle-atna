package net.ihe.gazelle.xua.provider;

import net.ihe.gazelle.common.LinkDataProvider;
import net.ihe.gazelle.xua.model.AncillaryTransaction;
import net.ihe.gazelle.xua.model.ServiceProviderTestCase;
import net.ihe.gazelle.xua.model.ServiceProviderTestInstance;
import org.kohsuke.MetaInfServices;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by aberge on 07/06/17.
 */
@MetaInfServices(LinkDataProvider.class)
public class XUATestDataLinkProvider implements LinkDataProvider{

    public static final String XUATESTING_BASE_URL = "xuatesting/";
    private static List<Class<?>> supportedClasses;

    static {
        supportedClasses = new ArrayList<Class<?>>();
        supportedClasses.add(ServiceProviderTestInstance.class);
        supportedClasses.add(ServiceProviderTestCase.class);
        supportedClasses.add(AncillaryTransaction.class);
    }

    @Override
    public List<Class<?>> getSupportedClasses() {
        return supportedClasses;
    }

    @Override
    public String getLabel(Object object, boolean showDetails) {
        StringBuilder label = new StringBuilder();
        if (object instanceof ServiceProviderTestInstance){
            ServiceProviderTestInstance ti = (ServiceProviderTestInstance) object;
            label.append('#');
            label.append(ti.getId());
            if (showDetails){
                label.append(" (");
                label.append(ti.getTestCase().getKeyword());
                label.append(')');
            }
        } else if (object instanceof ServiceProviderTestCase){
            ServiceProviderTestCase test = (ServiceProviderTestCase) object;
            label.append(test.getKeyword());
        } else if (object instanceof AncillaryTransaction){
            AncillaryTransaction transaction = (AncillaryTransaction) object;
            label.append(transaction.getKeyword());
            if (showDetails){
                label.append(" - ");
                label.append(transaction.getName());
            }
        }
        return label.toString();
    }

    @Override
    public String getLink(Object object) {
        StringBuilder link = new StringBuilder(XUATESTING_BASE_URL);
        if (object instanceof ServiceProviderTestCase){
            ServiceProviderTestCase test = (ServiceProviderTestCase) object;
            link.append("testCaseDetails.seam?id=");
            link.append(test.getId());
        } else if (object instanceof ServiceProviderTestInstance){
            ServiceProviderTestInstance ti = (ServiceProviderTestInstance) object;
            link.append("testInstanceDetails.seam?id=");
            link.append(ti.getId());
        } else if (object instanceof AncillaryTransaction){
            AncillaryTransaction transaction = (AncillaryTransaction) object;
            link.append("transactionDetails.seam?id=");
            link.append(transaction.getId());
        }
        return link.toString();
    }

    @Override
    public String getTooltip(Object object) {
        StringBuilder tooltip = new StringBuilder();
        if (object instanceof ServiceProviderTestInstance){
            ServiceProviderTestInstance ti = (ServiceProviderTestInstance) object;
            tooltip.append("Status: ");
            tooltip.append(ti.getTestStatus());
        } else if (object instanceof ServiceProviderTestCase){
            ServiceProviderTestCase test = (ServiceProviderTestCase) object;
            tooltip.append(test.getDescription());
        } else if (object instanceof AncillaryTransaction){
            AncillaryTransaction transaction = (AncillaryTransaction) object;
            tooltip.append(transaction.getDescription());
        }
        return tooltip.toString();
    }
}
