package net.ihe.gazelle.signature.validator.core;

/**
 * <p>SignatureValidatorConstants class.</p>
 *
 * @author abe
 * @version 1.0: 11/04/18
 */

class SignatureValidatorConstants {

    static final String CERT_VALID_MESSAGE = "The certificate is valid in terms of dates";
    static final String CANONICALIZED_REFERENCED_CONTENT = "canonicalized_referenced_content";
    static final String REFERENCE_CHECK_OK_MESSAGE = "Reference successfully checked";
    static final String REFERENCE_CHECK_KO_MESSAGE = "Verification of reference failed";
    static final String CHECK_STRUCTUREDBODY_ID_MESSAGE = "The structuredBody element should have an ID attribute";
    static final String CHECK_MULTIPLEDOCSIGNATURE_PRESENT_MESSAGE = "hl7fi:multipleDocumentSignature element is present";
    static final String CHECK_SIGNATURETIMESTAMP_ID_MESSAGE = "The signatureTimestamp shall have an ID attribute";
    static final String CHECK_SIGNATURE_VALUE_NO_CERT = "check_signature_value_no_cert";
    static final String SIGNATURE_VALUE_VALID = "signature_value_valid";
    static final String CHECK_SIGNATURE_VALUE_NO_CERT_MESSAGE = "Cannot check the signature: neither a X509 certificate nor a public key is provided";
    static final String SIGNATURE_VALUE_VALID_MESSAGE = "The signature value is valid";
    static final String SIGNATURE_VALUE_INVALID = "signature_value_invalid";
    static final String SIGNATURE_VALUE_INVALID_MESSAGE = "The signature value is invalid";
    static final String CERT_ISSUER_DN = "cert_issuer_dn"; // introduces variable
    static final String CERT_SUBJECT_DN = "cert_subject_dn"; // introduces variable
    static final String CERT_VALID = "cert_valid";
    static final String CERT_NO_MORE_VALID = "cert_no_more_valid";
    static final String CERT_NO_MORE_VALID_MESSAGE = "The certificate is no more valid";
    static final String CERT_NOT_YET_VALID = "cert_not_yet_valid";
    static final String CERT_NOT_YET_VALID_MESSAGE = "The certificate is not yet valid";
    static final String CERT_NOT_BEFORE = "cert_not_before";
    static final String CERT_NOT_AFTER = "cert_not_after";
    static final String DIGEST_VALUE_CORRECT = "digest_value_correct";
    static final String DIGEST_VALUE_CORRECT_MESSAGE = "The DigestValue is correct";
    static final String DIGEST_VALUE_MISTMATCH = "digest_value_mistmatch";
    static final String DIGEST_VALUE_MISTMATCH_MESSAGE = "The declared digest does not match the actual calculated digest";
    static final String DECLARED_DIGEST_VALUE = "declared_digest_value";
    static final String ACTUAL_CALCULATED_DIGEST = "actual_calculated_digest";
    static final String REFERENCE_CHECK_OK = "reference_check_ok";
    static final String REFERENCE_CHECK_KO = "reference_check_ko";
    static final String REFERENCE_CHECK_KO_W_MSG = "reference_check_ko_w_msg";
    static final String CHECK_STRUCTUREDBODY_ID = "check_structuredbody_id";
    static final String CHECK_MULTIPLEDOCSIGNATURE_PRESENT = "check_multipledocsignature_present";
    static final String CHECK_MULTIPLEDOCSIG_ID = "check_multipledocsig_id";
    static final String CHECK_MULTIPLEDOCSIG_ID_MESSAGE = "The multipleDocumentSignature should have an ID attribute";
    static final String CHECK_SIGNATURETIMESTAMP_PRESENT = "check_signaturetimestamp_present";
    static final String CHECK_SIGNATURETIMESTAMP_PRESENT_MESSAGE = "The signatureTimestamp element shall be present";
    static final String CHECK_SIGNATURETIMESTAMP_ID = "check_signaturetimestamp_id";
    static final String CHECK_KEYINFO_KO = "check_keyinfo_ko";
    static final String CHECK_KEYINFO_KO_MESSAGE = "KeyInfo element is required but missing";
    static final String CHECK_X_509_DATA_PRESENT = "check_x509data_present";
    static final String CHECK_X_509_DATA_PRESENT_MESSAGE = "A X509Data element is expected in the KeyInfo";
    static final String PRINT_DESCRIPTION = "print_description";

    private SignatureValidatorConstants(){

    }
}
