package net.ihe.gazelle.signature.validator.ws;

import net.ihe.gazelle.signature.exception.SignatureValidatorException;
import net.ihe.gazelle.signature.validator.core.SignatureValidator;
import net.ihe.gazelle.signature.validator.utils.XMLSignatureValidatorDescription;
import net.ihe.gazelle.validation.DetailedResult;
import net.ihe.gazelle.validation.exception.GazelleValidationException;
import net.ihe.gazelle.validation.model.ValidatorDescription;
import net.ihe.gazelle.validation.ws.AbstractModelBasedValidation;
import org.jboss.seam.annotations.Name;

import javax.ejb.Stateless;
import javax.jws.WebService;
import java.util.List;

/**
 * <p>SignatureValidator class.</p>
 *
 * @author abe
 * @version 1.0: 19/03/18
 */
@Stateless
@Name("ModelBasedValidationWS")
@WebService(name = "ModelBasedValidationWS",
        serviceName = "ModelBasedValidationWSService",
        portName = "ModelBasedValidationWSPort",
        targetNamespace = "http://ws.mb.validator.gazelle.ihe.net")
public class XMLSignatureValidatorWS extends AbstractModelBasedValidation {

    @Override
    protected String executeValidation(String documentToValidate, ValidatorDescription validatorDescription, boolean extracted) throws GazelleValidationException {
        if (XMLSignatureValidatorDescription.DEFAULT.equals(validatorDescription)) {
            SignatureValidator validator = new SignatureValidator();
            DetailedResult result ;
            try {
                result = validator.validate(documentToValidate);
            } catch (SignatureValidatorException e) {
                throw new GazelleValidationException("Cannot perform validation with validator " + validatorDescription.getDescriminator(), e);
            }
            return SignatureValidator.getDetailedResultAsString(result);
        } else {
            throw new GazelleValidationException("Cannot perform validation with validator " + validatorDescription.getDescriminator());
        }
    }

    @Override
    protected ValidatorDescription getValidatorByOidOrName(String oidOrName) {
        return XMLSignatureValidatorDescription.getEntryByOidOrName(oidOrName);
    }

    @Override
    protected List<ValidatorDescription> getValidatorsForDescriminator(String descriminator) {
        return XMLSignatureValidatorDescription.getValidatorsForDescriminator(descriminator);
    }

    @Override
    public String about(){
        String disclaimer = "This webservice is developed by IHE-Europe / Gazelle team. The aim of this validator is to validate Digital signature of XML documents.\n";
        disclaimer = disclaimer + "For more information please contact the manager of the Gazelle project: eric.poiseau@inria.fr";
        return disclaimer;
    }

    @Override
    protected String buildReportOnParsingFailure(GazelleValidationException e, ValidatorDescription selectedValidator) {
        return null;
    }

}
